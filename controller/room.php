<?php

namespace Controller;

use Model\RoomModel;
use System\Controller;
use System\KomA;

class Room extends Controller
{
    public $model;

    public function __construct()
    {
        $this->model = new RoomModel();
    }

    public function defaultAction()
    {
        return 'view';
    }

    public function view($id = NULL)
    {
        if ($id == NULL) {
            $data = $this->model->getRoomTypeList();
            $view = $this->render('view/room/room.php', [
                'system' => KomA::app(),
                'data' => $data,
            ], TRUE);
            $this->render('view/layout/main.php', [
                'system' => KomA::app(),
                'viewContent' => $view,
            ]);
        } else {
            $data = $this->model->getRoomDetail($id);
            $view = $this->render('view/room/roomdetail.php', [
                'system' => KomA::app(),
                'data' => $data,
            ], TRUE);
            $this->render('view/layout/main.php', [
                'system' => KomA::app(),
                'viewContent' => $view,
            ]);
        }
    }

    public function add()
    {
        if (empty($_POST)) {
            $data = $this->model->getRoomTypeList();
            $view = $this->render('view/room/addroom.php', [
                'system' => KomA::app(),
                'data' => $data,
            ], TRUE);
            $this->render('view/layout/main.php', [
                'system' => KomA::app(),
                'viewContent' => $view,
            ]);
        } else {
            $data = $this->model->addRoom($_POST['name'], $_POST['type'], $_POST['status']);
            if ($data == TRUE)
                echo "success";
            else
                echo "unsuccess";
        }
    }


    public function status()
    {
        $requestJSON = $_GET['json'];
        $data = $this->model->getRoomList();
        if (is_null($requestJSON) || !filter_var($requestJSON, FILTER_VALIDATE_BOOLEAN)) {
            $view = $this->render('view/room/viewroomstatus.php', [
                'system' => KomA::app(),
                'data' => $data,
            ], TRUE);
            $this->render('view/layout/main.php', [
                'system' => KomA::app(),
                'viewContent' => $view,
            ]);
        } else
            echo json_encode($data);
    }

    public function edit($id)
    {
        if (empty($_POST)) {
            $data[] = $this->model->getRoomTypeList();
            $data[] = $this->model->getRoomID($id);

            $view = $this->render('view/room/editroom.php', [
                'system' => KomA::app(),
                'data' => $data,
            ], TRUE);
            $this->render('view/layout/main.php', [
                'system' => KomA::app(),
                'viewContent' => $view,
            ]);
        } else {
            if($this->model->editRoom($id, $_POST['name'], $_POST['type'], $_POST['status']) == TRUE)
              KomA::app()->redirect('http://it.hotel/room/type');
        }
    }

    public function delete()
    {
        $data = $this->model->deleteRoom($_POST['id']);
        if ($data == TRUE)
            echo "success";
        else
            echo "unsuccess";
    }

    public function type($action, $id = NULL)
    {
        switch ($action) {
            case "add":
                if (empty($_POST)) {
                    $view = $this->render('view/room/addroomtype.php', [
                        'system' => KomA::app(),
                        'data' => $data,
                    ], TRUE);
                    $this->render('view/layout/main.php', [
                        'system' => KomA::app(),
                        'viewContent' => $view,
                    ]);
                } else {
                    $data = $this->model->addRoomType($_POST['name'], $_POST['price'], $_POST['feature'], $_FILES['input2']);
                    var_dump($data);
                }
                break;
            case "edit":
                echo "edit";
                break;
            case "details":
                if ($id == NULL) {
                    header('Location: http://it.hotel/room/type');
                } else {
                    $data = $this->model->getRoomDetails($id);
                    $view = $this->render('view/room/viewroom.php', [
                        'system' => KomA::app(),
                        'data' => $data,
                    ], TRUE);
                    $this->render('view/layout/main.php', [
                        'system' => KomA::app(),
                        'viewContent' => $view,
                    ]);
                }
                break;
            case "delete":
                $data = $this->model->deleteRoomType($_POST['id']);
                if ($data == TRUE)
                    echo "success";
                else
                    echo "unsuccess";
                break;
            default:
                $data = $this->model->getRoomDetail();
                $view = $this->render('view/room/viewroomtype.php', [
                    'system' => KomA::app(),
                    'data' => $data,
                ], TRUE);
                $this->render('view/layout/main.php', [
                    'system' => KomA::app(),
                    'viewContent' => $view,
                ]);
        }
    }

    public function gallery($action, $id = NULL)
    {
        switch ($action) {
            case "add":
                if (empty($_POST)) {
                    $data = $this->model->getRoomTypeList();
                    $view = $this->render('view/room/addroomgallery.php', [
                        'system' => KomA::app(),
                        'data' => $data,
                    ], TRUE);
                    $this->render('view/layout/main.php', [
                        'system' => KomA::app(),
                        'viewContent' => $view,
                    ]);
                } else {
                    $data = $this->model->addRoomGallery($_POST['type'], $_FILES['input2']);

                }
                break;
            case "edit":
                echo "edit";
                break;
            case "details":
                if ($id == NULL) {
                    header('Location: http://it.hotel/room/type');
                } else {
                    $data = $this->model->getRoomGallery($id);
                    $view = $this->render('view/room/viewroomgallery.php', [
                        'system' => KomA::app(),
                        'data' => $data,
                    ], TRUE);
                    $this->render('view/layout/main.php', [
                        'system' => KomA::app(),
                        'viewContent' => $view,
                    ]);
                }
                break;
            case "delete":
                echo "delete";
                break;
            default:
                header('Location: http://it.hotel/room/type');
        }
    }

}

?>
