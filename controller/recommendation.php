<?php

namespace Controller;

use Model\RecommendationModel;
use System\Controller;
use System\KomA;


class Recommendation extends Controller
{
    public $model;

    /**
     * Recommendation constructor.
     *
     * Method ini hanya menginisialisasikan sebuah objek baru $model.
     * Constructor ini tidak terlalu penting dan dapat disesuaikan dengan
     * kebutuhan anda.
     */
    public function __construct()
    {
        $this->model = new RecommendationModel();
    }

    /**
     * defaultAction
     *
     * defaultAction hanya menentukan action apa yang harus diambil
     * jika pengguna tidak menentukan action yang ingin diakses. Jika
     * ada cara penyelesaian yang lebih baik, boleh diubah.
     * @return string
     */
    public function defaultAction()
    {
        return 'index';
    }

    /**
     * Fungsi index
     *
     * Fungsi index dan lainnya yang bersifat public adalah action yang dapat
     * diakses oleh pengguna. Parameter yang terdapat pada fungsi dapat diambil
     * dari alamat web yang diberikan. Misalnya http://example.com/recommendation/index/1
     * . Jika action index memiliki sebuah parameter $a, maka nilai $a ketika action
     * index adalah 1.
     */
    public function index()
    {
        $view = $this->render("view/maps/recommendation.php", [
            'system' => KomA::app(),
        ], TRUE);
        $this->render("view/layout/main.php", [
            'viewContent' => $view,
            'system' => KomA::app(),
        ]);
    }

    public function get_category()
    {
        echo json_encode($this->model->getCategoryList());
    }

    public function get_list()
    {
        if (empty($_GET['CatID']) || is_null($_GET['CatID']))
            $_GET['CatID'] = -1;
        if (is_null($_GET['Name']) || empty($_GET['Name']))
            $_GET['Name'] = "";
        if (is_null($_GET['Range'])) {
            $_GET['Range']['Min'] = 0;
            $_GET['Range']['Max'] = -1;
        }
        echo json_encode([
            'list' => $this->model->getByCategoryID($_GET['CatID'], $_GET['Name'], $_GET['Range']['Min'], $_GET['Range']['Max']),
//            'minDistance' => $this->model->getMinDistance($_GET['CatID'], $_GET['Name']),
//            'maxDistance' => $this->model->getMaxDistance($_GET['CatID'], $_GET['Name']),
            'minDistance' => $this->model->getMinDistance(),
            'maxDistance' => $this->model->getMaxDistance(),
        ]);
    }

    public function manage($page = 1)
    {
        if ($page < 1)
            KomA::app()->redirect("recommendation/manage/1");
        $tempat = $this->model->getAll(5, $page);
        $view = $this->render("view/maps/index.php", [
            'system' => KomA::app(),
            'tempat' => $tempat,
            'paging' => [
                'page' => $page,
                'total' => $this->model->getCount(),
            ]
        ], TRUE);
        $this->render("view/layout/main.php", [
            'viewContent' => $view,
            'system' => KomA::app(),
        ]);
    }

    public function insert()
    {
        if ($_POST) {
            if (! is_bool($this->model->save($_POST['Nama'], $_POST['Lat'], $_POST['Lng'], $_POST['Alamat'], $_POST['Deskripsi'],
                $_POST['Jarak'], $_POST['Nilai_Prioritas'], $_POST['Kategori'])))
                KomA::app()->redirect(KomA::app()->site_url('recommendation/manage'));
            else
                KomA::app()->redirect(KomA::app()->site_url('recommendation/insert'));
        } else {
            $view = $this->render("view/maps/input.php", [
                'system' => KomA::app(),
                'kategori' => $this->model->getCategoryList(),
            ], TRUE);
            $this->render("view/layout/main.php", [
                'viewContent' => $view,
                'system' => KomA::app(),
            ]);
        }
    }

    public function delete($id)
    {
      $this->model->delete($id);
      KomA::app()->redirect(KomA::app()->site_url('recommendation/manage'));
    }

    public function edit($id = NULL)
    {
      if($_POST) {
        $this->model->edit($id, $_POST['Nama'], $_POST['Lat'], $_POST['Lng'], $_POST['Alamat'], $_POST['Deskripsi'],
            $_POST['Jarak'], $_POST['Nilai_Prioritas'], $_POST['Kategori']);
        KomA::app()->redirect(KomA::app()->site_url('recommendation/manage'));
      } else {
        $data = $this->model->selectByID($id);
        if($data === FALSE) {
            KomA::app()->redirect(KomA::app()->site_url('recommendation/manage'));
        } else {
            $view = $this->render("view/maps/edit.php", [
                'system' => KomA::app(),
                'data' => $data,
                'kategori' => $this->model->getCategoryList(),
                'selectedKategori' => $this->model->selectCategory($id),
            ], TRUE);
            $this->render("view/layout/main.php", [
                'viewContent' => $view,
                'system' => KomA::app(),
            ]);
        }
      }

    }

//    public function review() {
//        $view = $this->render("view/maps/index.php", [
//            'system' => KomA::app(),
//        ], TRUE);
//        $this->render("view/layout/main.php", [
//            'viewContent' => $view,
//            'system' => KomA::app(),
//        ]);
//    }

}

?>
