<?php

namespace Controller;

use Model\PayrollhrdModel;
use System\Controller;
use System\KomA;

class Payrollhrd extends Controller
{
    public $payrollhrdmodel;

    public function defaultAction() { return 'index'; }
	
	
	
    public function __construct()	
	{
        $this->payrollhrdmodel = new PayrollhrdModel();
    }
	
	public function index() {
		$system = KomA::app();
		if(is_null($system->get_worker_model()))
			$system->redirect($system->site_url('controlpanel/login'));
		else if($system->get_worker_model()->getAccessLevel() == 0 || $system->get_worker_model()->getAccessLevel() == 2) {
			$staff = $this->payrollhrdmodel->getByUsername(KomA::app()->get_worker_model()->getUsername());
		$report = isset($_GET['report'])?$_GET['report']:NULL;
		if($report==1){
			$this->payrollhrdmodel->pegawai_report($staff->ID);
		}
			$view = $this->render('view/payrollhrd/data_pegawai.php', [
				'system' => KomA::app(),
				'staff' => $staff,
		    ],true);
			$this->render('view/layout/main.php', [
				'system' => KomA::app(),
				'viewContent' => $view,
			]);
		} else {
			$system->redirect($system->site_url('payrollhrd/lists'));
		}
	}
	
	 public function mulaisesi() {
			session_start();
			return;
	} 

	public function lists() {
	$system= KomA::app();
		
       $showby = isset($_GET['showby'])?$_GET['showby']:NULL;
       $cari = isset($_GET['cari'])?$_GET['cari']:NULL;
	   if(!is_null(KomA::app()->get_worker_model()) && KomA::app()->get_worker_model()->getUsername() == 'Wendys') {
		   
		$staffs = $this->payrollhrdmodel->selectAll($showby,$cari);
    
		include 'view/payrollhrd/staff.php';
	   }
	   else {
		   $this->akhirisesi(); 
	   }
    }
	public function sallary() {
       $id = isset($_GET['id'])?$_GET['id']:NULL;
	   session_start();
       
	   if(!is_null(KomA::app()->get_worker_model()) && KomA::app()->get_worker_model()->getUsername() == 'Wendys') {
        $Nama_Pengguna = '';
        $Nama_Lengkap = '';
        $deskripsi='';
		$gaji=0;
        $errors = array();

        if ( isset($_POST['form-submitted']) ) {
            $Nama_Pengguna       = isset($_POST['Nama_Pengguna']) ?   $_POST['Nama_Pengguna']  :NULL;
            $Nama_Lengkap      = isset($_POST['Nama_Lengkap'])?   $_POST['Nama_Lengkap'] :NULL;
            $deskripsi     = isset($_POST['deskripsi'])?   $_POST['deskripsi'] :NULL;
            $gaji    = isset($_POST['gaji'])? $_POST['gaji']:NULL;

            try {
                $this->payrollhrdmodel->insertSallary($id,$deskripsi,$gaji);
            } catch (\Exception $e) {
                $errors = $e->getErrors();
            }
        }
		
		$staff = $this->payrollhrdmodel->selectById1($id);

        include 'view/payrollhrd/sallary.php';
		
	   }
	   else{
		    $this->akhirisesi();
		   
	   }
	}

    public function saveContact() {
		$system= KomA::app();
		session_start();
		
	  if(!is_null(KomA::app()->get_worker_model()) && KomA::app()->get_worker_model()->getUsername() == "Wendys") {
        $title = 'Add new staff';

        $Nama_Pengguna = '';
        $Nama_Lengkap = '';
        $Kata_Sandi = '';
        $Tanggal_Lahir = '';
		$Jenis_Kelamin = '';
        $Alamat = '';
        $Anggota_sejak = '';
		$forget_key=0;
		$status=1;
		$Id_Posisi=0;

        $errors = array();

        if ( isset($_POST['form-submitted']) ) {

            $Nama_Pengguna       = isset($_POST['Nama_Pengguna']) ?   $_POST['Nama_Pengguna']  :NULL;
            $Nama_Lengkap      = isset($_POST['Nama_Lengkap'])?   $_POST['Nama_Lengkap'] :NULL;
            $Kata_Sandi      = isset($_POST['Kata_Sandi'])?   $_POST['Kata_Sandi'] :NULL;
			$Tanggal_Lahir       = $_POST['Tahun'].'-'.$_POST['Bulan'].'-'.$_POST['Tanggal'];
            $Jenis_Kelamin      = isset($_POST['Jenis_Kelamin'])?   $_POST['Jenis_Kelamin'] :NULL;
            $Alamat    = isset($_POST['Alamat'])? $_POST['Alamat']:NULL;
			$Divisi    = isset($_POST['Divisi'])? $_POST['Divisi']:NULL;
			$Posisi = isset($_POST['Posisi'])? $_POST['Posisi']:NULL;
           // $Id_Posisi    = isset($_POST['Id_Posisi'])? $_POST['Id_Posisi']:NULL;


            try {
                $this->payrollhrdmodel->insert($Nama_Pengguna, $Nama_Lengkap, $Kata_Sandi, $Tanggal_Lahir,$Jenis_Kelamin,$Alamat,$Divisi,$Posisi);
				$this->akhirisesi();
                return;
				
            } catch (\Exception $e) {

            }
        }

        include 'view/payrollhrd/add.php';
		}
		
	  else {
		   $this->akhirisesi();
		   
	   }
    }
	
	 public function update() {
		$system= KomA::app();
		$ID = isset($_GET['id'])?$_GET['id']:NULL;
	
		session_start();
		if(KomA::app()->get_worker_model()->getUsername() == 'Wendys') {
       
		if ( isset($_POST['form-submitted']) ) {

           
            $Nama_Lengkap      = isset($_POST['Nama_Lengkap'])?   $_POST['Nama_Lengkap'] :NULL;
            $Kata_Sandi      = isset($_POST['Kata_Sandi'])?   $_POST['Kata_Sandi'] :NULL;
			$Tanggal_Lahir       = $_POST['Tahun'].'-'.$_POST['Bulan'].'-'.$_POST['Tanggal'];
            $Jenis_Kelamin      = isset($_POST['Jenis_Kelamin'])?   $_POST['Jenis_Kelamin'] :NULL;
            $Alamat    = isset($_POST['Alamat'])? $_POST['Alamat']:NULL;
			$Divisi    = isset($_POST['Divisi'])? $_POST['Divisi']:NULL;
			$Posisi = isset($_POST['Posisi'])? $_POST['Posisi']:NULL;
		


            try {
				
                $this->payrollhrdmodel->update($ID, $Nama_Lengkap, $Kata_Sandi, $Tanggal_Lahir,$Jenis_Kelamin,$Alamat,$Divisi,$Posisi);
				
				//$this->akhirisesi();
                //return;
				
            } catch (\Exception $e) {

            }
        }
		
		else {
			
			$staff = $this->payrollhrdmodel->selectById($ID);

		}

        include 'view/payrollhrd/update.php';
		}
		
	  else {
		   $this->akhirisesi();
		  
	   }
    }

    public function deleteGaji() {
		 $system= KomA::app();
        $id = isset($_GET['id'])?$_GET['id']:NULL;
		session_start();
        if ( !$id ) {
            header('Location:'.$system->site_url('payrollhrd/index'));
        }

        $this->payrollhrdmodel->delete($id);

        $this->akhirisesi();
    }

    public function showStaff() {
		 $system= KomA::app();
        $id = isset($_GET['id'])?$_GET['id']:NULL;
		$status = isset($_GET['status'])?$_GET['status']:NULL;
		
		session_start();
        if ( !$id ) {
            header('Location:'.$system->site_url('payrollhrd/index'));
        }
		
		if(KomA::app()->get_worker_model()->getUsername() == 'Wendys'){
			$staff = $this->payrollhrdmodel->selectById($id,$status);
			
			include 'view/payrollhrd/profil_pegawai.php';
		}
		
		else {
			$this->akhirisesi();
		   
		   header('Location:'.$system->site_url('payrollhrd/index'));
		}
    }
	
	
	public function get_suggest(){
		$q=$_GET['q'];
		$pegawai=$this->payrollhrdmodel->data_pegawai($q);
		echo json_encode($pegawai);
	}

    public function showError($title, $message) {
        include 'view/error.php';
    }

    public function akhirisesi() {
		session_start();
        $system= KomA::app();

		session_unset();

		session_destroy();
		$system->redirect($system->site_url('payrollhrd/index'));
	}
}

?>
