<?php

	namespace Controller;
	
	use System\Controller;
	use System\KomA;
	
	class ControlPanel extends Controller {
		
		public function defaultAction() { return 'login'; }
		
		public function login()	{
			$system = KomA::app();
			if( isset($_POST['Nama_Pengguna']) && isset($_POST['Kata_Sandi']) ) {
				if($system->set_worker_user($_POST['Nama_Pengguna'], $_POST['Kata_Sandi'])) {
					$system->redirect($system->site_url('payrollhrd/index'));
					// Sementara, link ke payroll
				} else {
					$this->redirect($system->site_url('controlpanel/login'));
				}
			} else {
				if(is_null($system->get_worker_model())) {
					$view = $this->render('view/login.php', [
						'system' => KomA::app(),
					], TRUE);
					$this->render('view/layout/main.php', [
						'system' => KomA::app(),
						'viewContent' => $view,
					]);
				} else {
					// Tampilan awal control panel
				}
			}
		}
		
		public function logout() {
			KomA::app()->clear_session();
			KomA::app()->redirect(KomA::app()->base_url());
			
		}
	}