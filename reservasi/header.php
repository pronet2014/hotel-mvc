<html>
<head>
	<title>KOM-A HOTEL RESORT - BOOK NOW!</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/stylee.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/form.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/bookcss.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="js/jquery1.min.js"></script>
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css" media="all" />
	
		<!-- start menu -->
	<link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="js/megamenu.js"></script>
	<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
		<!-- end start menu -->
	
		<!--start slider -->
    <link rel="stylesheet" href="css/fwslider.css" media="all">
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/css3-mediaqueries.js"></script>
    <script src="js/fwslider.js"></script>
		<!--end slider -->
		
	<script src="js/jquery.easydropdown.js"></script>
</head>
<body id="bo">
<div class="wrapper">
	<!--TOP HEADER-->
	<div class="navbar nav navbar-warning">
		<div class="wrap"> 
			<div class="header-top-left">
				<div class="box">
					<img class="call" src="picture/images/numbercall.png" height="30px">
				</div>
				<div class="clear">
				</div>
			</div>
			<div class="cssmenu">
				<ul>
					<li><a href="#"></a></li> |
					<li><a href="login.html">Log In</a></li> |
					<li><a href="sign-up.html">Sign Up</a></li> |
					<li><a href="#" style="background-color:black;padding:2px;">BOOK NOW</a></li> |
				</ul>
			</div>
			<div class="clear">
			</div>
		</div>
	</div>
	<!--end TOP HEADER-->
	
	<!--BOTTOM HEADER-->
	<div class="header-bottom">
	    <div class="wrap">
			<div class="header-bottom-left">
				<div class="logo">
					<a href="#"><img src="picture/images/log.png" alt="" height="50px"/></a>
				</div>
				<div class="menu">
					<ul class="megamenu skyblue">
						<li><a href="index.html">HOME</a></li>
						<li><a class="color1" href="recommendation.html">RECOMMENDATION</a></li>
						<li><a class="color2" href="room.html">ROOM</a></li>
						<li><a class="color3" href="facility.html">FACILITIES</a></li>
						<li><a class="color4" href="about.html">ABOUT US</a></li>
					</ul>
				</div>
			</div>
			<div class="header-bottom-right">
				<div class="search">	  
					<input type="text" name="s" class="textbox" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
					<input type="submit" value="Subscribe" id="submit" name="submit">
					<div id="response"></div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<!--end BOTTOM HEADER-->
	
	<!-- start slider -->
    <div id="fwslider">
        <div class="slider_container">
            <div class="slide"> 
                <!-- Slide image -->
                    <img src="picture/pict/ban3.jpg" alt=""/>
                <!-- /Slide image -->
                <!-- Texts container -->
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h1 class="title">WELCOME to</h4>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
                 <!-- /Texts container -->
            </div>
            <!-- /Duplicate to create more slides -->
            <div class="slide">
                <img src="picture/pict/ban2.jpg" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                       <!-- Text title -->
                        <h1 class="title">WELCOME to</h4>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
            </div>
            <!--/slide -->
			 <!-- /Duplicate to create more slides -->
            <div class="slide">
                <img src="picture/pict/ban1a.jpg" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                       <!-- Text title -->
                        <h1 class="title">WELCOME to</h4>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
            </div>
            <!--/slide -->
        </div>
        <div class="timers"></div>
        <div class="slidePrev"><span></span></div>
        <div class="slideNext"><span></span></div>
    </div>
	<!--end SLIDER-->
	
	<!--list HEADER-->
	<div class="header-list">
 	</div>
	<!--end list HEADER-->
