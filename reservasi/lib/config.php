 	<?php
$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = '';
$dbname = 'hotelkoma';

// Fungsi untuk koneksi ke database { menggunakan MySQLi Object Oriented }
$db = new mysqli($dbhost,$dbuser,$dbpass,$dbname);

// Check hasil fungsi koneksi berhasil atau tidak

// Jika terjadi Error
if ($db->connect_error) {
    
    // Sistem dihentikan
    die('Terjadi Kegagalan : '. $db->connect_error );
}
?>