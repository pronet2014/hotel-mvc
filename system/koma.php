<?php

namespace System;

use Model\WorkerModel;
use \mysqli;

/**
 * Class KomA
 *
 * Class KomA digunakan untuk mengakses fungsi umum yang digunakan
 * dan komponen yang digunakan pada aplikasi ini. Hanya satu objek
 * KomA yang diinsialisasikan pada satu eksekusi (singleton).
 * @package System
 *
 * @property WorkerModel $workerModel
 * @property mysqli $db
 */
class KomA
{
    private $webConfig, $dbConfig;

    private static $instance;
    private $currentController, $currentAction;

    private $db;
    private $workerModel;

    /**
     * KomA constructor.
     *
     * Method bersifat private karena class ini tidak dapat
     * digunakan pada semua class lainnya karena class ini
     * hanya boleh diinisialisasikan sekali dalam satu
     * eksekusi, yaitu dari fungsi app().
     * @see KomA::app()
     *
     */

    private function __construct() { }

    /**
     * app()
     *
     * Fungsi statik yang akan mengembalikan objek yang telah
     * diinisialisasikan untuk satu eksekusi. Jika objek $instance
     * belum dibuat, maka sistem akan membuat sebuah objek KomA baru.
     * @return KomA
     */
    public static function app()
    {
        if (! isset(static::$instance)) {
            static::$instance = new KomA();
            static::$instance->initClass();
        }
        return static::$instance;

    }

    /**
     * Method initClass
     *
     * Method ini melakukan autoload terhadap semua class controller, model, dan
     * class yang terdapat pada sistem. Class hanya akan diambil jika class tersebut
     * ingin digunakan.
     * @throws \Exception
     */
    private function initClass() {

        // spl_autoload yang terdapat pada Windows dan Linux berbeda
        // Problem : http://stackoverflow.com/questions/26146152/spl-autoload-fault-on-remote-server
        // Solution : http://php.net/manual/en/function.spl-autoload-register.php#111875
        spl_autoload_register(
          function ($pClassName) {
            spl_autoload(strtolower(str_replace("\\", "/", $pClassName)));
          }
        );

        if (file_exists('system/config/settings.php'))
            $this->webConfig = (include 'system/config/settings.php');
        if (file_exists('system/config/database.php')) {
            $this->dbConfig = (include 'system/config/database.php');
            $this->init_db();
        }
        session_start();
        if( isset($_SESSION['worker_username']))
            $this->workerModel = new WorkerModel($_SESSION['worker_username']);
        $this->routeURI();
    }

    /**
     * __destruct() akan menutup koneksi driver MySQLi demi efisiensi
     */
    public function __destruct()
    {
        if(! is_null($this->db))
            $this->db->close();
    }

    final public function __clone()
    {
        throw new Exception('This is a Singleton. Clone is forbidden');
    }

    final public function __wakeup()
    {
        throw new Exception('This is a Singleton. __wakeup usage is forbidden');
    }

    /**
     * Fungsi base_url
     *
     * Fungsi base_url akan mengembalikan setelan baseUrl yang telah tersedia pada
     * system/config/settings.php. Jika baseUrl pada file settings.php tidak diset,
     * maka sistem akan mengembalikan alamat server.
     * @return string
     */
    public function base_url() {
        if(isset($this->webConfig['baseUrl']))
            return $this->webConfig['baseUrl'];
        else
            return $_SERVER['SERVER_ADDR'];
    }

    /**
     * Fungsi site_url
     *
     * Fungsi ini akan mengembalikan link untuk controller/action yang ingin dibuat.
     * Misalnya, jika parameter $url bernilai site/index, maka fungsi site_url akan
     * memberikan link yang mengarah kepada controller site dengan action index.
     * @param $url string
     * @return string
     */
    public function site_url($url) {
        $e = explode('/', $url);
        if(count($e) == 0)
            return $this->base_url();
        if(class_exists("\\Controller\\".$e[0]))
            return $this->base_url()."/".implode('/', $e);
        else
            return $this->base_url();
    }

    /**
     * Method redirect
     *
     * Method ini hanya mempermudah developer untuk mengarahkan pengguna kepada link
     * yang ingin dituju. Misalkan, anda ingin mengarahkan pengguna ke example.com, maka
     * anda dapat langsung memanggil fungsi ini dengan argumen $url = http://example.com
     * @param $url string
     */
    public function redirect($url) {
        if(filter_var($url, FILTER_VALIDATE_URL) == TRUE)
            header("Location: ".$url);
    }

    /**
     * Method init_db()
     *
     * Method ini akan membuat sebuah objek mysqli untuk akses ke database yang ingin diakses.
     * Setelan database dapat diubah pada system/config/database.php
     * @throws \Exception
     */
    private function init_db() {
        if (!isset($this->db) || $this->db === NULL)
            $this->db = new \mysqli($this->dbConfig['DB_SERVER'], $this->dbConfig['DB_USERNAME'],
                $this->dbConfig['DB_PASSWORD'], $this->dbConfig['DB_NAME']);

        if (mysqli_connect_errno()) {
            $this->show_error("Error connecting to database.", "Failed to load database. ".mysqli_connect_error());
            throw new \Exception("Error connecting to database. " . mysqli_connect_error());
        }
    }

    /**
     * db()
     *
     * Mengambil objek database yang tersimpan pada KomA
     *
     * @return mysqli
     * @see mysqli
     * @see KomA
     */
    public function db() {
        if(is_null($this->db))
            init_db();
        return $this->db;
    }

    /**
     * Method routeURI()
     *
     * Method ini digunakan untuk mengarahkan pengguna ke controller/action yang ingin dituju.
     * Langkah kerja fungsi berikut:
     * 1. Pecahkan link yang ingin diakses dengan explode
     * 2. Jika pengguna tidak menentukan controller yang ingin diakses, redirect pengguna ke
     *    controller default yang telah ditentukan
     * 3. Jika controller tersedia, cek apakah method yang ingin diakses tersedia/tidak. Jika
     *    tidak, arahkan ke defaultAction jika disediakan.
     * 4. Simpan nama controller dan action yang diakses dan pecahkan argumen yang dikirim oleh
     *    pengguna jika tersedia. Argumen tersebut akan dikirim ke action yang ingin dituju.
     * @return mixed
     */
    private function routeURI()
    {
        $route = explode("/", parse_url($_SERVER['PATH_INFO'], PHP_URL_PATH));
        $errorMessage = NULL;
        if(count($route) <= 1 || empty($route[1])) {
            if(isset($this->webConfig['defaultController']) && !empty($this->webConfig['defaultController'])) {
                $route[1] = $this->webConfig['defaultController'];
                $this->redirect($this->site_url($this->webConfig['defaultController']));
            }
            $errorMessage = "No default controller for your web system. Please contact administrator.";

        }
        if(!empty($route[1]) && class_exists('\\Controller\\'.$route[1])) {
            $tujuanDalamString = "\\Controller\\".$route[1];
            $objekTujuan = new $tujuanDalamString();

            if(empty($route[2]) && method_exists($objekTujuan, 'defaultAction'))
                $route[2] = $objekTujuan->defaultAction();
            else
                $errorMessage = "No default action set for your request. Please contact administrator.";

            if(method_exists($objekTujuan, $route[2])) {
                $argument = array_splice($route, 3);
                $this->currentController = strtolower($route[1]);
                $this->currentAction = strtolower($route[2]);
                return call_user_func_array([$objekTujuan, $route[2]], $argument);
            }
        }
        $this->show_error("Failed to route your request.", $errorMessage);

    }

    /**
     * show_error
     *
     * Method ini hanya bantuan sementara jika anda ingin mengenali lokasi error
     * dengan kata-kata yang anda inginkan.
     * @param $title string
     * @param $message string
     */
    public function show_error($title, $message = NULL) {
        if(is_null($message))
            $message = (isset($this->webConfig['defaultErrorMsg']) ? $this->webConfig['defaultErrorMsg'] : "We're so sorry!");
        include "view/error.php";
    }

    public function is_logged() {
        return ! is_null($this->workerModel) && ! is_null($this->workerModel->getAccessLevel());
    }

    public function get_worker_model() {
        return $this->workerModel;
    }

    public function set_worker_user($username, $password) {
        if(is_null($this->workerModel))
            $this->workerModel = new WorkerModel();
        if($this->workerModel->verifyUser($username, $password)) {
            $this->workerModel->loadUser($username);
            $_SESSION['worker_username'] = $username;
            return TRUE;
        }
        return FALSE;
    }

    public function clear_session() {
        session_unset();
        session_destroy();
    }
	/* public function Login() {
		$system= KomA::app();
		$Nama_Pengguna = isset($_POST['Nama_Pengguna'])?$_POST['Nama_Pengguna']:NULL;
		$Kata_Sandi = isset($_POST['Kata_Sandi'])?$_POST['Kata_Sandi']:NULL;
		$report = isset($_GET['report'])?$_GET['report']:NULL;
		$id = isset($_GET['id'])?$_GET['id']:NULL;
		 session_start();
		if(isset($_SESSION['Nama_Pengguna'])){
			$Nama_Pengguna=$_SESSION['Nama_Pengguna'];
			$Kata_Sandi=$_SESSION['Kata_Sandi'];
		}
        if ( !$Nama_Pengguna || !$Kata_Sandi ) {
             header('Location:'.$system->site_url('payrollhrd/index'));
        }

        $staff = $system->selectForLogin($Nama_Pengguna,$Kata_Sandi,$report,$id);


		if($staff ===false){
			throw new \Exception('Nama_Pengguna and Kata_Sandi wrong');
		}
		else{
                       if($staff->Akses_Level==0 || $staff->Akses_Level==2) {
                $view = $this->render('view/payrollhrd/data_pegawai.php', [
                     'system' => KomA::app(),
                    'staff' => $staff,
               ],true);
                 $this->render('view/layout/main.php', [
                    'system' => KomA::app(),
                     'viewContent' => $view,
                 ]);
             }
             else
                header('Location:'.$system->site_url('payrollhrd/lists'));


	}

    }	

	*/
}
