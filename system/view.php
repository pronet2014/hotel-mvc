<?php

namespace System;

/**
 * Class View
 *
 * Class view hanya digunakan untuk mengasingkan data-data
 * yang dipakai oleh Controller dari View sehingga sistem
 * dapat memilih data yang perlu dikirim kepada View. Cara
 * ini juga dapat membantu sistem mengambil layout.
 * @package System
 */
class View
{
    private $data, $viewFile;

    /**
     * render()
     *
     * Fungsi render berfungsi untuk memberikan konten atau menampilkan
     * view tertentu. Fungsi ini menggunakan output buffer untuk
     * mencegah view yang ingin dirender ditampilkan kepada pengguna.
     * @param $returnView boolean Menentukan apakah view yang dirender
     * perlu dikembalikan sebagai string atau langsung ditampilkan kepada pengguna
     * @return string | bool
     */
    public function render($returnView) {
        extract($this->data);

        ob_start();

        if (file_exists($this->viewFile))
            require $this->viewFile;

        if($returnView)
            return ob_get_clean();
        else
            echo ob_get_clean();
    }

    /**
     * initClass
     *
     * Menyimpan nama file view dan data yang akan ditampilkan ke dalam
     * objek class ini.
     *
     * @param $file string Nama file view yang akan dirender
     * @param $data array Data yang akan digunakan atau diberikan kepada view.
     * @return bool
     */
    public function initClass($file, $data) {
        if(file_exists($file)) {
            $this->viewFile = $file;
            $this->data = $data;
            return true;
        } else
            return false;
    }

}