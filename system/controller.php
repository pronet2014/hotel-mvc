<?php
/**
 * Created by PhpStorm.
 * User: rex
 * Date: 6/20/16
 * Time: 7:39 PM
 */

namespace System;

/**
 * Class Controller
 * @package System
 * 
 * Class ini adalah parent dari class controller lainnya.
 * Dibuat hanya untuk memberikan fungsi render kepada semua
 * controller. 
 */
class Controller
{
    /**
     * @param string $viewFile Nama file view yang akan dirender
     * @param array $data Data yang akan digunakan pada view tersebut
     * @param bool $return Menentukan apakah hasil render akan langsung ditampilkan atau dikembalikan
     * @return string Hasil render jika $return bernilai TRUE
     */
    protected function render($viewFile, $data = [ ], $return = FALSE) {
        $view = new View();
        $view->initClass($viewFile, $data);
        return $view->render($return);
    }
}