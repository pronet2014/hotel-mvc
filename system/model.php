<?php

namespace System;

/**
 * Class Model
 *
 * Class model seharusnya adalah class yang menyediakan
 * data yang diperlukan oleh controller dalam memproses
 * permintaan pengguna. Pada class ini, Model didesain
 * untuk memiliki akses database tanpa harus selalu
 * mengakses objek KomA. Class ini juga dapat memiliki
 * method-method yang dapat membantu sistem dalam
 * melakukan pekerjaannya, seperti escape.
 * @package System
 * @property  $db mysqli
 */
class Model
{
    protected $db;
    
    public function __construct()
    {
        $system = KomA::app();
        $this->db = $system->db();
    }
    public function escape($a){
        return $this->db->real_escape_string($a);
    }

}
