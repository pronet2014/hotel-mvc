<?php

namespace Model;

use System\Model;

class PayrollhrdModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public function getByUsername($username) {
		$dbres = $this->db->prepare("select * from seluruh where Nama_Pengguna='$username' order by ID desc");
		if($dbres->execute()) {
            $r = $dbres->get_result();
            if($r !== FALSE) {
                while($row = $r->fetch_assoc()) {
                   $arrRes = (object) $row;
                }
                return $arrRes;
            }
        }

        return FALSE;
	}
	public function pegawai_report($id) {
		return $this->db->query("UPDATE karyawan set status=1 WHERE ID='$id'");
	}
	
	public function data_pegawai($q){
		$dbres = $this->db->prepare("SELECT Nama_Pengguna from karyawan where Nama_Pengguna like '%$q%' limit 5");
		if($dbres->execute()) {
            $r = $dbres->get_result();

            if($r !== FALSE) {
                while($row = $r->fetch_assoc()) {
                   $arrRes[] = $row;
				  
                }

                return $arrRes;
            }
        }

        return FALSE;
	}
	public function selectAll($showby , $cari) {
		  	
        if(!isset($cari)) {
			if ( !isset($showby)) {
				$showby = "All";
			}
			$dbOrder =  $this->escape($showby);
			if($showby=="All"){
				$dbres = $this->db->prepare("SELECT * FROM seluruh order by Id_gaji");
			}
			else{
				$dbres = $this->db->prepare("SELECT * FROM seluruh where Nama_Divisi like '%$dbOrder%'");
			}
		}
		else if(isset($cari)) {
			if ( !isset($showby)) {
				$showby = $cari;
			}
			$dbOrder =  $this->escape($cari);
			if($showby=="All"){
				$dbres = $this->db->prepare("SELECT * FROM seluruh order by Id_gaji");
			}
			else{
				$dbres = $this->db->prepare("SELECT * FROM seluruh where Nama_Pengguna like '%$dbOrder%'");
			}
		}
		 
		if($dbres->execute()) {
            $r = $dbres->get_result();

            if($r !== FALSE) {
                while($row = $r->fetch_assoc()) {
                   $arrRes[] = (object) $row;
				  
                }

                return $arrRes;
            }
        }

        return FALSE;
    }

    

    public function selectById($id, $status)
    {
		
        $dbId = $this->escape($id);
		$dbstatus = $this->escape($status);
		if($dbstatus == 2){
		$this->db->query("UPDATE gaji set Status=1 WHERE ID='$dbId'");
        $this->db->query("UPDATE gaji set Tanggal_Pembayaran=NOW() where ID='$dbId'");
		$this->db->query("UPDATE karyawan set Status=0 WHERE ID= (select Id_Karyawan from gaji where ID='$dbId')");
		}
		else{}
		$dbres = $this->db->prepare("SELECT * FROM seluruh WHERE Id_gaji='$dbId'");
 
		if($dbres->execute()) {
			
            $r = $dbres->get_result();
          
			if($r !== FALSE) {
                while($row = $r->fetch_assoc()) {
                    $arrRes = (object)$row;
                }
				
                return $arrRes;
            }
        }
        return FALSE;
		
    }

    public function selectById1($id)
    {
        $dbId = $this->escape($id);
        $dbres = $this->db->prepare("SELECT * FROM karyawan WHERE ID='$dbId'");

        if ($dbres->execute()) {
            $r = $dbres->get_result();
            if ($r !== FALSE) {
                while ($row = $r->fetch_assoc()) {
                    $arrRes = (object)$row;
                }
                return $arrRes;
            }
        }
        return FALSE;

    }

    public function insert( $Nama_Pengguna, $Nama_Lengkap, $Kata_Sandi, $Tanggal_Lahir,$Jenis_Kelamin,$Alamat,$Divisi,$Posisi ) {

        $dbNama_Pengguna = ($Nama_Pengguna != NULL) ? "'" . $this->escape($Nama_Pengguna) . "'" : 'NULL';
        $dbNama_Lengkap = ($Nama_Lengkap != NULL) ? "'" . $this->escape($Nama_Lengkap) . "'" : 'NULL';
        $dbKata_Sandi = ($Kata_Sandi != NULL) ? "'" . $this->escape($Kata_Sandi) . "'" : 'NULL';
        $dbTanggal_Lahir = ($Tanggal_Lahir != NULL) ? "'" . $this->escape($Tanggal_Lahir) . "'" : 'NULL';
        $dbJenis_Kelamin = ($Jenis_Kelamin != NULL) ? "'" . $this->escape($Jenis_Kelamin) . "'" : 'NULL';
        $dbAlamat = ($Alamat != NULL) ? "'" . $this->escape($Alamat) . "'" : 'NULL';
        $dbDivisi = ($Divisi != NULL) ? "'" . $this->escape($Divisi) . "'" : 'NULL';
        $dbPosisi = ($Posisi != NULL) ? "'" . $this->escape($Posisi) . "'" : 'NULL';


        if ($Posisi = 'manager') {
            $this->db->query("INSERT INTO posisi (Nama_Posisi,Akses_Level,Gaji_Pokok,Id_Divisi) VALUES ($dbPosisi,2,7000000,$dbDivisi)");
        } else if ($Posisi = 'member') {
            $this->db->query("INSERT INTO posisi (Nama_Posisi,Akses_Level,Gaji_Pokok,Id_Divisi) VALUES ($dbPosisi,0,2000000,$dbDivisi)");
        }
		else if($Posisi='member')
		{
		$this->db->query("INSERT INTO posisi (Nama_Posisi,Akses_Level,Gaji_Pokok,Id_Divisi) VALUES ($dbPosisi,0,2000000,$dbDivisi)");
		}
		$this->db->query("INSERT INTO karyawan (Nama_Pengguna, Nama_Lengkap, Kata_Sandi, Tanggal_Lahir,Jenis_Kelamin,Alamat,Anggota_sejak,Id_Posisi) VALUES ($dbNama_Pengguna,$dbNama_Lengkap,$dbKata_Sandi,$dbTanggal_Lahir,$dbJenis_Kelamin,$dbAlamat,now(),Last_Insert_id())");
		$this->db->query("INSERT INTO gaji (Id_Karyawan) VALUES (Last_Insert_id())");
		return $this->db->insert_id;
    }
	
	public function update($ID, $Nama_Lengkap, $Kata_Sandi, $Tanggal_Lahir,$Jenis_Kelamin,$Alamat,$Divisi,$Posisi ) {
		$Posisi=strtolower($Posisi);
		$dbID = $this->escape($ID);
        $dbNama_Lengkap = $this->escape($Nama_Lengkap);
        $dbKata_Sandi = $this->escape($Kata_Sandi);
        $dbTanggal_Lahir = $this->escape($Tanggal_Lahir);
		$dbJenis_Kelamin = $this->escape($Jenis_Kelamin);
        $dbAlamat = $this->escape($Alamat);
        $dbDivisi = $this->escape($Divisi);
        $dbPosisi = $this->escape($Posisi);
		
			if($Posisi='manager'){
				$this->db->query("UPDATE posisi set Nama_Posisi='$dbPosisi',Akses_Level=2,Gaji_Pokok=7000000,Id_Divisi='$dbDivisi' WHERE ID =(select Id_Posisi from karyawan where ID=(select Id_Karyawan from gaji where ID='$dbID'))");
				
				$this->db->query("UPDATE karyawan set Nama_Lengkap='$dbNama_Lengkap',Kata_Sandi='$dbKata_Sandi',Tanggal_Lahir='$dbTanggal_Lahir',Jenis_Kelamin='$dbJenis_Kelamin',Alamat='$dbAlamat' WHERE ID ='$dbID'");
			}
			else if($Posisi='member') {
				$this->db->query("UPDATE posisi set Nama_Posisi='$dbPosisi',Akses_Level=0,Gaji_Pokok=2000000,Id_Divisi='$dbDivisi' WHERE ID =(select Id_posisi from karyawan where ID='$dbID')");
				$this->db->query("UPDATE karyawan set Nama_Lengkap='$dbNama_Lengkap',Kata_Sandi='$dbKata_Sandi',Tanggal_Lahir='$dbTanggal_Lahir',Jenis_Kelamin='$dbJenis_Kelamin',Alamat='$dbAlamat' WHERE ID='$dbID'");
			}
			
		
	}

    public function insertSallary($id, $deskripsi, $gaji)
    {
        $dbId = ($id != NULL) ? "'" . $this->escape($id) . "'" : 'NULL';
        $dbdeskripsi = ($deskripsi != NULL) ? "'" . $this->escape($deskripsi) . "'" : 'NULL';
        $dbgaji = ($gaji != NULL) ? "'" . $this->escape($gaji) . "'" : 'NULL';

        $this->db->query("INSERT INTO gaji (Tambahan_Gaji,Deskripsi,Id_Karyawan) VALUES ($dbgaji,$dbdeskripsi,$dbId)");
        return $this->db->insert_id;
    }

    public function delete($id)
    {
        $dbId = $this->escape($id);
        $this->db->query("DELETE FROM gaji WHERE id=$dbId");
    }


}
