<?php
/**
 * Created by PhpStorm.
 * User: rex
 * Date: 6/22/16
 * Time: 4:57 PM
 */

namespace Model;

use System\Model;

class WorkerModel extends Model
{
    private $id, $username, $access_level;

    public function __construct($username = NULL) {
        parent::__construct();
        if(!is_null($username) && !empty($username))
            $this->loadUser($username);
    }

    /**
     * @param $username Username karyawan yang akan dicek
     * @param $password Password karyawan yang akan dicek
     * @return bool Hasil pengecekan username dan password karyawan
     */
    public function verifyUser($username, $password) {
        $query = $this->db->prepare("SELECT Nama_Pengguna FROM karyawan WHERE Nama_Pengguna = ? AND Kata_Sandi = ?");
        if(! is_bool($query) && !empty($username) && !empty($password)) {
            $query->bind_param('ss', $username, $password);

            if ($query->execute()) {
                $result = $query->get_result();
                return ($result->num_rows > 0);
            }
        }
        return FALSE;
    }

    public function loadUser($username) {
        $query = $this->db->prepare("SELECT ID, Nama_Pengguna, (SELECT Akses_Level FROM posisi WHERE posisi.ID = karyawan.Id_Posisi LIMIT 1) AS 'AccessLevel', (SELECT Tanggal_Pembayaran FROM gaji WHERE Id_Karyawan = karyawan.ID LIMIT 1) AS 'InsertDate' FROM karyawan WHERE Nama_Pengguna = ? ORDER BY InsertDate DESC LIMIT 1");
        if( ! is_bool($query) && !empty($username)) {
            $query->bind_param('s', $username);

            if($query->execute()) {
                $result = $query->get_result();
                if($result->num_rows > 0) {
                    $data = $result->fetch_assoc();
                    $this->id= $data[ID];
                    $this->username = $data[Nama_Pengguna];
                    $this->access_level = $data[AccessLevel];
                    return TRUE;
                }
            }
        }
        return FALSE;
    }
	
	
	
    public function getAccessLevel() {
        return $this->access_level;
    }
	
	public function getUsername() {
		return $this->username;
	}

    public function getID() { return $this->id; }
	
}