<?php
/**
 * Created by PhpStorm.
 * User: rex
 * Date: 6/18/16
 * Time: 7:14 PM
 */

namespace Model;

use System\Model;

class RecommendationModel extends Model
{
    public function getAll($limit = -1, $page = -1) {
        if ($limit > 0 && $page > 0) {
            $realOffset = $limit * ($page - 1);
        } else if($limit > 0 && $page < 0) {
            $realOffset = 0;
            $limit = 1280000;
        }
        $q = $this->db->query("SELECT * FROM tempat".($limit > 0 && $page > 0 ? " LIMIT $realOffset, $limit" : ""));

        while ($row = $q->fetch_assoc()) {
            $arrRes[$row['Id_Rekomendasi']] = $row;
        }
        return $arrRes;
    }

    public function getByCategoryID($catID = -1, $nameLike = "", $min = 0, $max = -1, $orderBy = "Id", $limit = -1, $page = -1)
    {
        if ($limit > 0 && $page > 0) {
            $realOffset = $limit * ($page - 1);
        }
        $q = $this->db->prepare("SELECT * FROM tempat_kategori JOIN tempat ON (tempat_kategori.Id_Rekomendasi = tempat.Id_Rekomendasi)
            JOIN kategori ON (tempat_kategori.Id_Kategori = kategori.Id_Kategori) WHERE tempat_kategori.Id_Kategori " . ($catID == -1 ? ' >= ' : ' = ') .
            " ?" . " AND Nama LIKE CONCAT('%', ?, '%') AND Jarak >= ? AND " . ($max == -1 ? " ? " : " Jarak <= ? ") . "ORDER BY " . $this->escape($orderBy) .
            ($limit > 0 && $page > 0 ? " LIMIT $realOffset, $limit" : ""));
        $q->bind_param('isii', $catID, $nameLike, $min, $max);
        if ($q->execute()) {
            $r = $q->get_result();
            if ($r !== FALSE) {
                while ($row = $r->fetch_assoc()) {
                    $arrRes[$row['Id_Rekomendasi']] = $row;
                }
                return $arrRes;
            }
        }
        return FALSE;
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function getCategoryList($limit = 0, $page = -1)
    {
        $q = $this->db->prepare("SELECT * FROM kategori " . (($limit <= 0) ? "" : "LIMIT ?,?"));
        if ($limit > 0) {
            $realOffset = $limit * $page;
            $q->bind_param('ii', $realOffset, $limit);
        }
        if ($q->execute()) {
            $r = $q->get_result();
            if ($r !== FALSE) {
                while ($row = $r->fetch_assoc()) {
                    $arrRes[$row['Id_Kategori']] = $row;
                }
                return $arrRes;
            }
        }
        return FALSE;
    }

    public function getMaxDistance($CatID = -1, $Like = "")
    {
        $q = $this->db->prepare("SELECT max(Jarak) FROM tempat WHERE tempat.Id_Rekomendasi IN (SELECT Id_Rekomendasi FROM tempat_kategori WHERE Id_Kategori " . ($CatID == -1 ? '>=' : '=') . " ?) AND tempat.Nama LIKE CONCAT('%', ?, '%')");
        $q->bind_param('is', $CatID, $Like);
        if ($q->execute()) {
            $r = $q->get_result();
            if ($r->num_rows > 0)
                return $r->fetch_array(MYSQLI_NUM)[0];
            else
                return FALSE;
        }
    }

    public function getMinDistance($CatID = -1, $Like = "")
    {
        $q = $this->db->prepare("SELECT min(Jarak) FROM tempat WHERE tempat.Id_Rekomendasi IN (SELECT Id_Rekomendasi FROM tempat_kategori WHERE Id_Kategori " . ($CatID == -1 ? '>=' : '=') . " ?) AND tempat.Nama LIKE CONCAT('%', ?, '%')");
        $q->bind_param('is', $CatID, $Like);
        if ($q->execute()) {
            $r = $q->get_result();
            if ($r->num_rows > 0)
                return $r->fetch_array(MYSQLI_NUM)[0];
            else
                return FALSE;
        }
    }

    public function getCount()
    {
        $q = $this->db->query("SELECT count(Id_Rekomendasi) FROM tempat");
        $r = $q->fetch_array(MYSQLI_NUM)[0];
        return $r;
    }

    public function save($name, $lat, $lng, $addr, $desc, $dist, $priority, $cat)
    {
        $this->db->begin_transaction();
        $q = $this->db->prepare("INSERT INTO tempat(Nama, Latitude, Longitude, Alamat, Deskripsi, Jarak, Nilai_Prioritas)
             VALUES(?, ?, ?, ?, ?, ?, ?)");
        $q->bind_param('sddssii', $name, $lat, $lng, $addr, $desc, $dist, $priority);
        if($q->execute()) {
            $id = $q->insert_id;
            $saveQuery = $this->db->prepare("INSERT INTO tempat_kategori(Id_Rekomendasi, Id_Kategori) VALUES(?, ?)");
            foreach($cat as $data) {
                $saveQuery->bind_param('ii', $id, $data);
                if(! $saveQuery->execute()) {
                    $this->db->rollback();
                    return FALSE;
                }
            }
            return $this->db->commit();
        } else
            return FALSE;
    }

    public function edit($id, $name, $lat, $lng, $addr, $desc, $dist, $priority, $kategori)
    {
        $this->db->begin_transaction();
        $q = $this->db->prepare("UPDATE tempat SET Nama = ?,  Latitude = ?, Longitude = ?, Alamat = ?, Deskripsi = ? , Jarak = ? ,Nilai_Prioritas = ? WHERE Id_Rekomendasi = ?");
        $q->bind_param('sddssiii', $name, $lat, $lng, $addr, $desc, $dist, $priority, $id);
        if($q->execute()) {
            $q2 = $this->db->prepare("DELETE FROM tempat_kategori WHERE Id_Rekomendasi = ?");
            $q3 = $this->db->prepare("INSERT INTO tempat_kategori(Id_Rekomendasi, Id_Kategori) VALUES(?, ?)");
            $q2->bind_param('i', $id);
            if($q2->execute()) {
                foreach($kategori as $data) {
                    $q3->bind_param('ii', $id, $data);
                    if(! $q3->execute()) {
                        $this->db->rollback();
                        return FALSE;
                    }
                }
                return $this->db->commit();
            }
        }
        $this->db->rollback();
        return FALSE;
    }

    public function delete($id)
    {
        $q = $this->db->prepare("DELETE FROM tempat WHERE Id_Rekomendasi=$id");
        // $q->bind_param('sddssii', $name, $lat, $lng, $addr, $desc, $dist, $priority);
        $q->execute();
    }

    public function selectByID($id)
    {
        $q = $this->db->prepare("SELECT * FROM tempat WHERE Id_Rekomendasi = ?");
        $q->bind_param('i', $id);
        if($q->execute()) {
            $r = $q->get_result();
            return $r->fetch_assoc();
        }
        return FALSE;
    }

    public function selectCategory($id) {
        $q = $this->db->query("SELECT Id_Kategori FROM tempat_kategori WHERE Id_Rekomendasi = ".intval($id));
        return $q->fetch_all(MYSQLI_NUM);
    }
}
