<?php

	namespace Model;

	use System\Model;

	class RoomModel extends Model {

		public $data;

		public function __construct() {
			parent::__construct();
		}

		public function getRoomTypeList() {
			$result = $this->db->query("SELECT id, name, price, picture FROM room_type");

			if($result->num_rows > 0) {
				while($rows = $result->fetch_assoc()) {
					$this->data[]=$rows;
				}
				return $this->data;
			} else {
				echo "Tidak ada data!";
			}
		}

		public function getRoomID($id) {
			return $this->data = $this->db->query("SELECT name AS room_name, type_id, status FROM room WHERE name='$id'")->fetch_assoc();
		}

		public function getRoomDetail($id = NULL) {
			if($id == NULL) {
				$result = $this->db->query("SELECT * FROM room_type");

				if($result->num_rows > 0) {
					while($rows = $result->fetch_assoc()) {
						$this->data[]=$rows;
					}
				} else {
					echo "Tidak ada data!";
				}
			} else {
				$this->data = $this->db->query("SELECT id AS type_id, price, feature FROM room_type WHERE name='".ucwords(preg_replace('/-/', ' ', $id))."'")->fetch_assoc();
				$result = $this->db->query("SELECT picture FROM room_gallery WHERE type_id=".$this->data[type_id]);
				while($rows = $result->fetch_assoc()) {
					$this->data[picture][]=$rows[picture];
				}
				$this->data[id] = ucwords(preg_replace('/-/', ' ', $id));
			}
			return $this->data;
		}

		public function getRoomDetails($id) {
			$result = $this->db->query("SELECT b.name, b.status FROM room_type a JOIN room b ON(a.id=b.type_id) WHERE a.name='".ucwords(preg_replace('/-/', ' ', $id))."'");

			$i = 0;
			while($rows = $result->fetch_assoc()) {
				$this->data[details][$i][name]=$rows[name];
				$this->data[details][$i][status]=$rows[status];
				$i++;
			}
			$this->data[id] = ucwords(preg_replace('/-/', ' ', $id));
			return $this->data;
		}

		public function getRoomList() {
			$result = $this->db->query("SELECT name, status FROM room");
			while($rows = $result->fetch_assoc()) {
				$this->data[]=$rows;
			}
			return $this->data;

		}

		public function getRoomGallery($id) {
			$result = $this->db->query("SELECT b.picture FROM room_type a JOIN room_gallery b ON(a.id=b.type_id) WHERE a.name='".ucwords(preg_replace('/-/', ' ', $id))."'");
			while($rows = $result->fetch_assoc()) {
				$this->data[picture][]=$rows[picture];
			}
			$this->data[id] = ucwords(preg_replace('/-/', ' ', $id));
			return $this->data;
		}

		public function addRoom($name, $type, $status) {
			$name = $this->escape($name);
			$type = $this->escape($type);
			$status = $this->escape($status);
			$query = $this->db->query("INSERT INTO room (name, type_id, status) VALUES ('$name', $type, $status)");
			if($query)
				return TRUE;
			else
				return FALSE;
		}

		public function editRoom($id, $name, $type, $status) {
			$query = $this->db->query("UPDATE room SET name = '$name', type_id = '$type', status= $status WHERE name = '$id'");
			if($query)
				return TRUE;
			else
				return FALSE;
		}

		public function addRoomType($name, $price, $feature, $input2) {
			$name = $this->escape($name);
			$price = $this->escape($price);
			$feature = $this->escape($feature);
			$filename = $input2['name'];
			$filename1 = explode(".", $filename)[0];
			$target = __DIR__."/../assets/images/room/".$filename;
		  if(move_uploaded_file($input2['tmp_name'], $target)) {
				$query = $this->db->query("INSERT INTO room_type (name, price, feature, picture) VALUES ('$name', $price, '$feature', '$filename1')");
				// if($query)
				// 	return TRUE;
				// else
				// 	return FALSE;
		  } else {
	      // unlink($target);
	      // return FALSE;
		  }
		}

		public function addRoomGallery($type, $input2) {
		 $type = $this->escape($type);

		 $target = [];
//		 $filenames = $input2['name'];
//		 $filenames1 = explode(".", $filenames)[0];
		 for($i=0; $i < count($input2['name']); $i++){
//			 var_dump($input2);
			 $target[$i] = __DIR__."/../assets/images/room/".$input2['name'][$i];
//			 $target[$i] = "/srv/school.local/".$input2['name'][$i];
//			 var_dump(move_uploaded_file($input2['tmp_name'][$i], $target[$i]));
//			 var_dump($input2['tmp_name'][$i]);
//			 var_dump($target[$i]);
//			 exit();
			 if(move_uploaded_file($input2['tmp_name'][$i], $target[$i])) {
				 $query = $this->db->query("INSERT INTO room_gallery (type_id, picture) VALUES ($type, '".$input2['name'][$i]."')");
			 } else {
				 // unlink($target);
				  return FALSE;
			 }
		 }
		return TRUE;
	 }

		public function deleteRoomType($id) {
			$query = $this->db->query("DELETE FROM room_type WHERE id='$id'");
			if($query)
				return TRUE;
			else
				return FALSE;
		}

		public function deleteRoom($id) {
			$query = $this->db->query("DELETE FROM room WHERE name='$id'");
			if($query)
				return TRUE;
			else
				return FALSE;
		}

	}
?>
