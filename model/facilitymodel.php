<?php

	namespace Model;

	use System\Model;

	class FacilityModel extends Model {

		public $data;

		public function __construct() {
			parent::__construct();
		}

		public function getFacilityList() {
			$result = $this->db->query("SELECT nama_fasilitas, Harga, Satuan, deskripsi, image FROM fasilitas");

			if($result->num_rows > 0) {
				while($rows = $result->fetch_assoc()) {
					$this->data[]=$rows;
				}
				return $this->data;
			} else {
				echo "Tidak ada data!";
			}
		}
		
			public function updateFacility($nama_facility, $harga, $satuan, $deskripsi,$gambar) {
			
			$dbId = $this->escape($id);
			$dbnama = ($nama_facility != NULL)?"'".$this->escape($nama_facility)."'":'NULL';
			$dbharga = ($harga != NULL)?"'".$this->escape($harga)."'":'NULL';
			$dbsatuan = ($satuan != NULL)?"'".$this->escape($satuan)."'":'NULL';
			$dbdeskripsi = ($deskripsi != NULL)?"'".$this->escape($deskripsi)."'":'NULL';
			$dbgambar = ($gambar != NULL)?"'".$this->escape($gambar)."'":'NULL';
		
			$this->db->query("update fasilitas set nama_fasilitas='$dbnama',Harga = 'dbharga', Satuan = '$dbsatuan', Deskripsi='$dbdeskripsi',image='dbgambar' where Id_fasilitas='$dbId'");
			return $this->db->update_id;
		}
		
		public function selectById1($id) {
			$dbId = $this->escape($id);
			$dbres =$this->db->prepare("SELECT * FROM fasilitas WHERE Id_fasilitas='$dbId'");

			if($dbres->execute()) {
				$r = $dbres->get_result();
				if($r !== FALSE) {
					while($row = $r->fetch_assoc()) {
						$arrRes = (object)$row;
					}
					return $arrRes;
				}
			}
			return FALSE;

		}
		
		public function deleteFacility($id)
		{
			$dbId = $this->escape($id);
			$this->db->query("delete from fasilitas WHERE Id_fasilitas='$dbId'");
			return;
		}
	}
?>
