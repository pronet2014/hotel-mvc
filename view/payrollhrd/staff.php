<?php
session_start();
?>
<html>
<head>
<?php
        $system = \System\KomA::app();
    ?>
	<title>KOM-A HOTEL RESORT</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	 <meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

	
  	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	
  	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link href="<?php echo $system->base_url() ?>/assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php echo $system->base_url() ?>/assets/css/form.css" rel="stylesheet" type="text/css" media="all" />
	<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="<?php echo $system->base_url() ?>/assets/js/jquery1.min.js"></script>


	<!--CSS-->
    <link rel="stylesheet" href="<?php echo $system->base_url() ?>/assets/css/bootstrap.css">

		<!-- start menu -->
	<link href="<?php echo $system->base_url() ?>/assets/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="<?php echo $system->base_url() ?>/assets/js/megamenu.js"></script>
	<script>$(document).ready(function(){
		$(".megamenu").megamenu();
		
		});</script>
		<!-- end start menu -->

		<!--start slider -->
    <link rel="stylesheet" href="<?php echo $system->base_url() ?>/assets/css/fwslider.css" media="all">
    <script src="<?php echo $system->base_url() ?>/assets/js/jquery-ui.min.js"></script>
    <script src="<?php echo $system->base_url() ?>/assets/js/css3-mediaqueries.js"></script>
    <script src="<?php echo $system->base_url() ?>/assets/js/fwslider.js"></script>
		<!--end slider -->

	<script src="<?php echo $system->base_url() ?>/assets/js/jquery.easydropdown.js"></script>
</head>
<body>
	<!--TOP HEADER-->
	<div class="header-top">
		<div class="wrap">
			<div class="header-top-left">
				<div class="box">
					<img class="call" src="<?php echo $system->base_url() ?>/assets/images/numbercall.png" height="30px">
				</div>
				<div class="clear">
				</div>
			</div>
			<div class="cssmenu">
				<ul>

					<li><a href="<?php echo $system->site_url('payrollhrd/akhirisesi')?>" style="background-color:black;padding:2px;">Log Out</a></li>
				</ul>
			</div>
			<div class="clear">
			</div>
		</div>
	</div>
	<!--end TOP HEADER-->

	<!--BOTTOM HEADER-->
	<div class="header-bottom">
	    <div class="wrap">
			<div class="header-bottom-left">
				<div class="logo">
					<a href="#"><img src="<?php echo $system->base_url() ?>/assets/images/log.png" alt="" height="50px"/></a>
				</div>
				<div class="menu">
					<ul class="megamenu skyblue">

					</ul>
				</div>
			</div>

			<div class="clear"></div>
		</div>
	</div>
	<!--end BOTTOM HEADER-->

    <!-- start slider -->
    <div id="fwslider">
        <div class="slider_container">
            <div class="slide">
                <!-- Slide image -->
                <img src="<?php echo $system->base_url() ?>/assets/images/pict/ban3.jpg" alt=""/>
                <!-- /Slide image -->
                <!-- Texts container -->
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h1 class="title">WELCOME to</h1>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
                <!-- /Texts container -->
            </div>
            <!-- /Duplicate to create more slides -->
            <div class="slide">
                <img src="<?php echo $system->base_url() ?>/assets/images/pict/ban2.jpg" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h1 class="title">WELCOME to</h1>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
            </div>
            <!--/slide -->
            <!-- /Duplicate to create more slides -->
            <div class="slide">
                <img src="<?php echo $system->base_url() ?>/assets/images/pict/ban1a.jpg" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h1 class="title">WELCOME to</h1>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
            </div>
            <!--/slide -->
        </div>
        <div class="timers"></div>
        <div class="slidePrev"><span></span></div>
        <div class="slideNext"><span></span></div>
    </div>
    <!--end SLIDER-->

	<!--list HEADER-->
	<div class="header-list">
 	</div>
	<!--end list HEADER-->
<br>

<div class="container">
  <ul class="nav nav-tabs" role="tablist">
    <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href=" ">
      Divisi <span class="caret"></span></a>
      <ul class="dropdown-menu" role="menu">
		<li><a href="<?php echo $system->site_url('payrollhrd/lists') ?>?showby=All">All</a></li>
        <li><a href="<?php echo $system->site_url('payrollhrd/lists') ?>?showby=Marketing">Marketing</a></li>
        <li><a href="<?php echo $system->site_url('payrollhrd/lists') ?>?showby=Front">Front Office</a></li>
        <li><a href="<?php echo $system->site_url('payrollhrd/lists') ?>?showby=House">HouseKepping</a></li>
        <li><a href="<?php echo $system->site_url('payrollhrd/lists') ?>?showby=Laundry">Laundry</a></li>
        <li><a href="<?php echo $system->site_url('payrollhrd/lists') ?>?showby=Enggineering">Enggineering & Maintenance</a></li>
        <li><a href="<?php echo $system->site_url('payrollhrd/lists') ?>?showby=Food">Food and Beverage</a></li>
        <li><a href="<?php echo $system->site_url('payrollhrd/lists') ?>?showby=Finance">Finance</a></li>
        <li><a href="<?php echo $system->site_url('payrollhrd/lists') ?>?showby=Personnel">Personnel</a></li>
        <li><a href="<?php echo $system->site_url('payrollhrd/lists') ?>?showby=Training">Training</a></li>
        <li><a href="<?php echo $system->site_url('payrollhrd/lists') ?>?showby=Security">Security</a></li>
      


      </ul>
    </li>
  </ul>
</div>
<br>
<script>
	$(document).ready(function(){
    $("input").on('keyup', function(){
        $.ajax({
			url: "<?php echo $system->site_url('payrollhrd/get_suggest')?>", 
			method: 'GET',
			data: {
				q: $("input[name=cari]").val()
			},
			dataType: 'json',
			success: function(result){
				console.log(result);
			   $('#suggestion').html('');
			   for(i=0;i<result.length;i++){
				   $('#suggestion').append('<a href="<?php echo $system->site_url('payrollhrd/lists')?>?cari=' + result[i].Nama_Pengguna +'">' + result[i].Nama_Pengguna + "</a><br>");
			   }
        }});
    });
});
</script>
	<div class="header-bottom-right">
				<div class="search staff">
					
					<form action="" method="GET">
					<input type="text" name="cari" class="textbox" value="Cari Karyawan" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" autocomplete=off>
					<input type="submit">
					<div id="suggestion"></div>
					<div id="response"></div>
					</form>
				</div>
			</div>
<div class="col-sm-12">
<div><a href="<?php echo $system->site_url('payrollhrd/saveContact')?>">Tambah karyawan baru</a></div>
<table  class="table table-striped table-bordered table-hover staffs ">
  <thead >
  <tr>
    <th><center>No</th>
    <th><center>Id</th>
    <th><center>Nama_Pengguna</th>
	<th><center>Kata_Sandi</th>
    <th><center>Anggota_Sejak</th>
    <th><center>Status</th>
    <th><center>Posisi</th>
    <th><center>Gaji</th>
    <th><center>Deskripsi</th>
	<th><center>Tambahan_Gaji</th>
    <th><center>Tanggal_Pembayaran</th>
  </tr>
  </thead>

  <tbody>
   <?php $i=1;foreach ($staffs as $staff): ?>
         
		 <tr class="active">
                    <td><?php print $i++; ?>
					</td>
					<td><?php print ($staff->ID); ?></td>
					<td><?php print htmlentities($staff->Nama_Pengguna); ?></a></td>
                    <td><?php print htmlentities($staff->Kata_Sandi); ?></td>
                    <td><?php print htmlentities($staff->Anggota_sejak); ?></td>
					<td><?php if($staff->status==1)echo "please check me"; else echo"i'm fine"; ?></td>
                    <td><?php print htmlentities($staff->Nama_Posisi); ?></td>
                    <td><?php print htmlentities($staff->Gaji_Pokok); ?></td>
					<td><?php print htmlentities($staff->Deskripsi); ?></td>
                    <td><?php print htmlentities($staff->Tambahan_Gaji); ?></td>
                    <td><?php print htmlentities($staff->Tanggal_Pembayaran); ?></td>
					<td><a href="<?php echo $system->site_url('payrollhrd/showStaff')?>?id=<?php print $staff->Id_gaji; ?>&status=<?php print $staff->Status_gaji; ?>"><button>lihat</button></a></td>
					<td><a href="<?php echo $system->site_url('payrollhrd/sallary')?>?id=<?php print $staff->ID; ?>"><button>gaji</button></a></td>
                    <td><a href="<?php echo $system->site_url('payrollhrd/deleteGaji')?>?id=<?php print $staff->Id_gaji; ?>" onclick="return confirm('Apakah anda yakin ingin menghapus data <?php echo $staff->Nama_Pengguna; ?>?')" ><button>hapus</button></a></td>

                </tr>
            <?php endforeach; ?>
  	
  </tbody> </center>
</table> </div>


	<!--footer
	<div class="footer">
    <div class="footer-bottom">
        <div class="copy">
            <p>© 2016 Template by. <a href="index.html" target="_blank">KOM-A TI USU 2014</a></p>
        </div>
        <img class="footer_logo" src="<?php echo $system->base_url() ?>/assets/images/logofooter.png">
        <div class="clear"></div>
    </div>
</div>-->
</body>
</html>