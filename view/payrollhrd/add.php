<html>
<head>
<?php
        $system = \System\KomA::app();
    ?>
	<title>KOM-A HOTEL RESORT</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	 <meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link href="<?php echo $system->base_url() ?>/assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php echo $system->base_url() ?>/assets/css/form.css" rel="stylesheet" type="text/css" media="all" />
	<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="<?php echo $system->base_url() ?>assets/js/jquery1.min.js"></script>
  	

	<!--CSS-->
    <link rel="stylesheet" href="<?php echo $system->base_url() ?>/assets/css/bootstrap.css">

		<!-- start menu -->
	<link href="<?php echo $system->base_url() ?>/assets/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="<?php echo $system->base_url() ?>/assets/js/megamenu.js"></script>
	<script>$(document).ready(function(){
		$(".megamenu").megamenu();
		
		});</script>
		<!-- end start menu -->

		<!--start slider -->
    <link rel="stylesheet" href="<?php echo $system->base_url() ?>/assets/css/fwslider.css" media="all">
    <script src="<?php echo $system->base_url() ?>/assets/js/jquery-ui.min.js"></script>
    <script src="<?php echo $system->base_url() ?>/assets/js/css3-mediaqueries.js"></script>
    <script src="<?php echo $system->base_url() ?>/assets/js/fwslider.js"></script>
		<!--end slider -->

	<script src="<?php echo $system->base_url() ?>/assets/js/jquery.easydropdown.js"></script>
</head>
<body>
	<!--TOP HEADER-->
	<div class="header-top">
		<div class="wrap">
			<div class="header-top-left">
				<div class="box">
					<img class="call" src="<?php echo $system->base_url() ?>/assets/images/numbercall.png" height="30px">
				</div>
				<div class="clear">
				</div>
			</div>
			<div class="cssmenu">
				<ul>

					<li><a href="<?php echo $system->site_url('payrollhrd/akhirisesi')?>" style="background-color:black;padding:2px;">Log Out</a></li>
				</ul>
			</div>
			<div class="clear">
			</div>
		</div>
	</div>
	<!--end TOP HEADER-->

	<!--BOTTOM HEADER-->
	<div class="header-bottom">
	    <div class="wrap">
			<div class="header-bottom-left">
				<div class="logo">
					<a href="#"><img src="<?php echo $system->base_url() ?>/assets/images/log.png" alt="" height="50px"/></a>
				</div>
				<div class="menu">
					<ul class="megamenu skyblue">

					</ul>
				</div>
			</div>

			<div class="clear"></div>
		</div>
	</div>
	<!--end BOTTOM HEADER-->

    <!-- start slider -->
    <div id="fwslider">
        <div class="slider_container">
            <div class="slide">
                <!-- Slide image -->
                <img src="<?php echo $system->base_url() ?>/assets/images/pict/ban3.jpg" alt=""/>
                <!-- /Slide image -->
                <!-- Texts container -->
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h1 class="title">WELCOME to</h1>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
                <!-- /Texts container -->
            </div>
            <!-- /Duplicate to create more slides -->
            <div class="slide">
                <img src="<?php echo $system->base_url() ?>/assets/images/pict/ban2.jpg" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h1 class="title">WELCOME to</h1>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
            </div>
            <!--/slide -->
            <!-- /Duplicate to create more slides -->
            <div class="slide">
                <img src="<?php echo $system->base_url() ?>/assets/images/pict/ban1a.jpg" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h1 class="title">WELCOME to</h1>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
            </div>
            <!--/slide -->
        </div>
        <div class="timers"></div>
        <div class="slidePrev"><span></span></div>
        <div class="slideNext"><span></span></div>
    </div>
    <!--end SLIDER-->

	<!--list HEADER-->
	<div class="header-list">
 	</div>
	<!--end list HEADER-->
<br>
<div class ="container">
<form class="form-horizontal" method="POST" action="">
  <fieldset>
    <legend>Form Karyawan</legend>
    <div class="form-group">
      <label class="col-md-2 control-label" for="inputUsername">Nama Pengguna</label>

      <div class="col-md-6">
        <input class="form-control" id="inputUsername" type="username" placeholder="Username" name="Nama_Pengguna">
      </div>
    </div>
	<div class="form-group">
      <label class="col-md-2 control-label" for="inputName">Nama Lengkap</label>

      <div class="col-md-6">
        <input class="form-control" id="inputName" type="name" placeholder="Full Name" name="Nama_Lengkap">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="inputPassword" onautocomplete="off">Kata Sandi</label>

      <div class="col-md-6">
        <input class="form-control" id="inputPassword" type="password" placeholder="Password" name="Kata_Sandi">
</div>
</div>
		<div class="form-group">
            <label class="col-md-2 control-label" for ="lblTglLahir">Tanggal Lahir</label>
            <div class="col-xs-2">
                <select class="form-control" name="Tanggal">
                    <option value="" disabled selected><b>Tanggal</b></option>
					<script>
						function formatInt(angka, panjang) {
							var stringHasil = "" + angka;
							while(stringHasil.length < panjang) {
								stringHasil = "0" + stringHasil;
							}
							return stringHasil;
						}
						
						$(document).ready( function() {
							// $(".megamenu").megamenu();
							var i;
							for(i = 1; i <= 31; i++) {
								$("select[name=Tanggal]").append("<option value=" + formatInt(i,2) + ">" + formatInt(i, 2) +"</option>");
							}
							for(i = 1960; i <= 2015; i++) {
								$("select[name=Tahun]").append("<option value=" + i + ">" + i + "</option>");
							}
						});
					
					</script>
                </select>
            </div>
            <div class="col-xs-2">
                <select class="form-control" name="Bulan">
                    <option value="" disabled selected><b>Bulan</b></option>
                    <option value="01">Januari</option>
                    <option value="02">Februari</option>
                    <option value="03">Maret</option>
                    <option value="04">April</option>
                    <option value="05">Mei</option>
                    <option value="06">Juni</option>
                    <option value="07">Juli</option>
                    <option value="08">Agustus</option>
                    <option value="09">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">Nopember</option>
                    <option value="12">Desember</option>
                </select>
            </div>
            <div class="col-xs-2">
                <select class="form-control" name="Tahun">
                    <option value="" disabled selected><b>Tahun</b></option>
                </select>
            </div>
			</div>
   <div class="form-group">
      <label class="col-md-2 control-label" for="inputAddress">Alamat</label>

      <div class="col-md-6">
        <input class="form-control" id="inputAddress" type="address" placeholder="Address" name="Alamat">	
      </div>
    </div>
	
	 <div class="form-group">
            <label class="col-md-2 control-label for"="lblGender">Jenis_Kelamin</label>
            <div class="col-xs-1">
                <label class="radio-inline">
                    <input type="radio" name="Jenis_Kelamin" value="Male"> Male
                </label>
            </div>
            <div class="col-xs-1">
                <label class="radio-inline">
                    <input type="radio" name="Jenis_Kelamin" value="Female"> Female
                </label>
            </div>
        </div>
		
		<div class="form-group">
	 <label class="col-md-2 control-label" for="select222">Divisi</label>

      <div class="col-md-6">
        <select class="form-control" id="select222" name=Divisi>
          <option value=1>Marketing Department</option>
          <option value=2>Front Office Department</option>
          <option value=3>House Kepping Department</option>
          <option value=4>Enggineering & Maintenance Department</option>
          <option value=5>Laundry Department</option>
		  <option value=6>Food and Beverage Department</option>
		  <option value=7>Finance Department</option>
		  <option value=8>Personnel Department</option>
		  <option value=9>Training Department</option>
		  <option value=10>Security Department</option>
        </select>
      </div>
    </div>
	
	 <div class="form-group">
      <label class="col-md-2 control-label">Posisi</label>

      <div class="col-md-6">
        <div class="radio radio-primary">
          <label>
            <input name="Posisi" id="optionsRadios1" type="radio" checked="" value="manager">
            Manager
          </label>
        </div>
        <div class="radio radio-primary">
          <label>
            <input name="Posisi" id="optionsRadios2" type="radio" value="member">
            Member
          </label>
        </div>
      </div>
    </div>
	
    <div class="form-group">
      <div class="col-md-10 col-md-offset-2">
   
        <button class="btn btn-primary" type="submit" name="form-submitted" onclick="return confirm('Apakah anda yakin ingin dengan data yang anda isi?')">Tambah</button>
      </div>
    </div>
  </fieldset>
</form>

	</div>
	<!--footer-->
	<div class="footer">
		<div class="footer-bottom">
			<div class="copy">
				<p>© 2016 Template by. <a href="index.html" target="_blank">KOM-A TI USU 2014</a></p>	
			</div>
			<img class="footer_logo" src="<?php echo $system->base_url() ?>/assets/images/logofooter.png">
			<div class="clear"></div>
		</div>
	</div>
	
</body>
</html>