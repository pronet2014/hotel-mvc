<div class="login">
    <div class="col_1_of_login span_1_of_login">
        <div class="login-title">
            <h4 class="title">
                <center>LOG IN</center>
            </h4>
            <div id="loginbox" class="loginbox">
                <form action="<?php echo $system->site_url('controlpanel/login') ?>" method="post" name="login"
                      id="login-form">
                    <fieldset class="input">
                        <p id="login-form-username">
                            <label for="modlgn_username">Nama_Pengguna</label>
                            <input id="modlgn_username" type="text" name="Nama_Pengguna" class="inputbox" size="18"
                                   autocomplete="off">
                        </p>
                        <p id="login-form-password">
                            <label for="modlgn_passwd">Kata_Sandi</label>
                            <input id="modlgn_passwd" type="password" name="Kata_Sandi" class="inputbox" size="18"
                                   autocomplete="off">
                        </p>
                        <div class="remember">
                            <p id="login-form-remember">
                                <label for="modlgn_remember"><a href="#">Forget Your Password ? </a></label>
                            </p>

                            <input type="submit" name="submit" class="button" value="Login"></a>
                            <div class="clear"></div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>