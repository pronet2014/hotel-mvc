<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <title>Application error</title>
</head>
<body>
<style>
    .error-box {
        border: 0.15em solid #333333;
        display: inline-block;
        margin-top: 10%;
        margin-bottom: 10%;
    }
    .error-container:before {
        content: "";
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-left: -1em;
    }

    .error-box .title {
        background: #333333;
        color: #EEEEEE;
        padding: 1em;
    }
    .error-box .message {
        padding: 1em;
        line-height: 150%;
        vertical-align: middle;
    }
</style>
<div class="container-fluid wrapper">
    <div class="row">
        <div class="error-container col-md-6 col-md-offset-3 text-center">
            <div class="error-box">
                <div class="title">
                    <h1><?php print htmlentities($title) ?></h1>
                </div>
                <div class="message">
                    <?php print htmlentities($message) ?>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
