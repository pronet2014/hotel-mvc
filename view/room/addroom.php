<!--main-->
<div class="main">
  <div class="container">
    <ol class="breadcrumb" style="margin-bottom: 15px;">
      <li><a href="<?php echo $system->base_url(); ?>">Home</a></li>
      <li><a href="<?php echo $system->base_url(); ?>/room">Kamar</a></li>
      <li class="active">Tambah</li>
    </ol>

    <div class="col-sm-offset-2 col-sm-10 clr" style="margin-bottom: 10px;">
      <h2>Tambah Kamar</h2>
      <div id="notification" class="alert" style="display: none"></div>
    </div>

    <form class="form-horizontal">
      <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Nama</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="name" name="name" placeholder="Nama Kamar" autocomplete="off">
        </div>
      </div>
      <div class="form-group">
        <label for="type" class="col-sm-2 control-label">Tipe</label>
        <div class="col-sm-10">
          <select class="form-control" id="type" name="type">
            <?php
              foreach($data as $room)
                echo "<option value=\"$room[id]\">$room[name]</option>";
            ?>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="status" class="col-sm-2 control-label">Status</label>
        <div class="col-sm-10">
          <div class="radio">
            <label>
              <input type="radio" name="status" value="1" checked>
              Tersedia
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="status" value="0">
              Tidak tersedia
            </label>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button id="submit" class="btn btn-default">Submit</button>
        </div>
      </div>
    </form>
	</div>
	<div class="clear"></div>
</div>
<!--end main-->

<script>
  $(document).ready(function(){
    $('button#submit').on('click', function() {
      if($('input#name').val()!="") {
        $.ajax( {
          type: "POST",
          url: location.href,
          data: $('form').serialize(),
          cache: false,
          success: function(data) {
            $("#notification").css("display", "block");
            if(data=="success") {
              $("#notification").addClass("alert-success");
              $("#notification").removeClass("alert-danger");
              $("#notification").html("<strong>Anda berhasil menambah data kamar!</strong>");
            } else if(data=="unsuccess") {
              $("#notification").addClass("alert-danger");
              $("#notification").removeClass("alert-success");
              $("#notification").html("<strong>Anda tidak berhasil menambah data kamar!</strong>");
            }
          }
        });
      }
      return false;
    });
  });
</script>
