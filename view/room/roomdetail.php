<!--main-->
<div class="main">
    <div class="wrap">

        <!-- mulai bagusan bagian ini -->
        <table width="100%">
            <tr>
                <td colspan="2" style="text-align: center; font-size: 2.5em;"><?php echo $data[id]; ?></td>
            </tr>
            <tr>
                <td width="40%" style="padding-right: 20px; text-align: right; padding-bottom: 10px;">Harga</td>
                <td >IDR <?php echo number_format($data[price], 0, "", "."); ?></td>
            </tr>
            <tr>
                <td style="padding-right: 20px; text-align: right">Fasilitas</td>
                <td>
                    <?php
                    $a = explode(', ', $data[feature]);
                    foreach ($a as $row) {
                        echo "<li>" . $row . "</li>";
                    }
                    ?>
                </td>
            </tr>
        </table>
        <!-- akhir bagusin bagian ini -->

        <?php foreach ($data[picture] as $row): ?>
            <div class="col_1_of_3">
                <?php echo "<img src='" . $system->base_url() . "/assets/images/room/" . $row . ".jpg' height='200px'>"; ?>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="clear"></div>
</div>
