<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/u/bs/dt-1.10.12/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/u/bs/dt-1.10.12/datatables.min.js"></script>

<!--main-->
<div class="main">
  <div class="container">
    <ol class="breadcrumb" style="margin-bottom: 15px;">
      <li><a href="<?php echo $system->base_url(); ?>">Home</a></li>
      <li><a href="<?php echo $system->base_url(); ?>/room/type">Tipe Kamar</a></li>
      <li class="active"><?php echo $data['id']; ?></a></li>
    </ol>

    <div class="col-sm-12 clr" style="text-align: center; margin-bottom: 10px;">
      <h2>Data Kamar</h2> <a href="<?php echo $system->base_url(); ?>/room/add">Tambah Kamar</a>
    </div>

		<table id="roomtype-tb" class="table table-striped table-bordered data">
			<thead>
				<tr><th>Nama Kamar</th><th>Status</th><th>Aksi</th></tr>
			</thead>
			<tbody>
				<?php
          foreach($data[details] as $row) {
            echo "<tr><td>$row[name]</td><td>";
            echo $row[status] == 1 ? "Tersedia" : "Tidak Tersedia";
            echo "</td><td><a href='".$system->base_url()."/room/edit/$row[name]'>Edit</a> | <a class='hapus' style=\"cursor: pointer;\" room=\"$row[name]\">Hapus</a></td></tr>";
          }
				?>
			</tbody>
		</table>
	</div>
	<div class="clear"></div>
</div>
<!--end main-->

<script>
  $(document).ready(function(){
    $('#roomtype-tb').DataTable();

    $('a.hapus').on('click', function() {
      if(confirm('Apakah Anda yakin akan menghapus kamar ini?') == true) {
        $.ajax( {
          type: "POST",
          url: '<?php echo $system->site_url('room/delete'); ?>',
          data: 'id='+$(this).attr('room'),
          cache: false,
          success: function(data) {
            if(data == "success")
              window.location.reload();
            else
              alert('Terdapat kesalahan sehingga tidak dapat melakukan penghapusan data!');
          }
        });
      }
    });
  });
</script>
