<link href="<?php echo $system->base_url(); ?>/assets/css/fileinput.min.css" rel="stylesheet" type="text/css" media="all" />

<!--main-->
<div class="main">
  <div class="container">
    <ol class="breadcrumb" style="margin-bottom: 15px;">
      <li><a href="<?php echo $system->base_url(); ?>">Home</a></li>
      <li><a href="<?php echo $system->base_url(); ?>/room/type">Tipe Kamar</a></li>
      <li class="active">Tambah</li>
    </ol>

    <div class="col-sm-offset-2 col-sm-10 clr" style="margin-bottom: 10px;">
      <h2>Tambah Tipe Kamar</h2>
      <div id="notification" class="alert" style="display: none"></div>
    </div>

    <form class="form-horizontal" method="post" enctype="multipart/form-data">
      <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Nama</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="name" name="name" placeholder="Nama Tipe Kamar" autocomplete="off">
        </div>
      </div>
      <div class="form-group">
        <label for="price" class="col-sm-2 control-label">Harga</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="price" name="price" placeholder="Harga" autocomplete="off">
        </div>
      </div>
      <div class="form-group">
        <label for="feature" class="col-sm-2 control-label">Fitur</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="feature" name="feature" placeholder="Fitur1, Fitur2, ..." autocomplete="off">
        </div>
      </div>
			<div class="form-group">
				<label for="input-2" class="col-sm-2 control-label">Gambar</label>
				<div class="col-sm-10">
					<input id="input-2" name="input2" type="file" class="file" data-show-caption="true" data-allowed-file-types='["image"]'>
				</div>
			</div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" class="btn btn-default">Submit</button>
        </div>
      </div>
    </form>
	</div>
	<div class="clear"></div>
</div>
<!--end main-->

<script src="<?php echo $system->base_url(); ?>/assets/js/fileinput.min.js"></script> 
<script>
  $(document).ready(function(){
		$("#input-2").fileinput({
      uploadAsync: false,
      uploadUrl: location.href,
      uploadExtraData: function() {
        return {
          name: $("input[name=name]").val(),
          price: $("input[name=price]").val(),
          feature: $("input[name=feature]").val()
        };
      }
		});
  });
</script>
