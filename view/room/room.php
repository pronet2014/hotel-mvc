<!--main-->
<div class="main">
    <div class="wrap">
        <div class="section group">
            <div class="cont">
                <h2 class="head" style="text-align: center;"></h2>
                <div class="top-box">
                    <?php foreach ($data as $room): ?>
                        <div class="col_1_of_3">
                            <div class="inner_content clearfix">
                                <div class="product_image">
                                    <img
                                        src="<?php echo $system->base_url(); ?>/assets/images/room/<?php echo $room[picture]; ?>.jpg"
                                        height="205px" alt=""/>
                                </div>
                                <div class="room-name">
                                    <a href="<?php echo $system->base_url(); ?>/room/view/<?php echo strtolower(preg_replace('/ /', '-', $room[name])); ?>"><?php echo $room[name]; ?></a>
                                </div>
                                <div class="price">
                                    IDR <?php echo number_format($room[price], 0, "", "."); ?>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<!--end main-->
