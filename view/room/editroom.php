<!--main-->
<div class="main">
  <div class="container">
    <ol class="breadcrumb" style="margin-bottom: 15px;">
      <li><a href="<?php echo $system->base_url(); ?>">Home</a></li>
      <li><a href="<?php echo $system->base_url(); ?>/room">Kamar</a></li>
      <li class="active">Edit</li>
    </ol>

    <div class="col-sm-offset-2 col-sm-10 clr" style="margin-bottom: 10px;">
      <h2>Edit Kamar</h2>
    </div>

    <form class="form-horizontal" method="post" action=>
      <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Nama</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="name" name="name" placeholder="Nama Kamar" value="<?php echo $data[1]['room_name']; ?>">
        </div>
      </div>
      <div class="form-group">
        <label for="type" class="col-sm-2 control-label">Tipe</label>
        <div class="col-sm-10">

          <select class="form-control" id="type" name="type">
            <?php
              foreach($data[0] as $room) {
                $a = ($data[1][type_id] == $room[id]) ? "selected" : "";
                echo "<option value=\"$room[id]\" $a>$room[name]</option>";
              }
            ?>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="status" class="col-sm-2 control-label">Status</label>
        <div class="col-sm-10">
          <div class="radio">
            <label>
              <input type="radio" name="status" value="1" <?php echo ($data[1][status] == 1) ? "checked" : ""; ?>>
              Tersedia
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="status" value="0" <?php echo ($data[1][status] == 0) ? "checked" : ""; ?>>
              Tidak tersedia
            </label>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button id="submit" class="btn btn-default">Submit</button>
        </div>
      </div>
    </form>
	</div>
	<div class="clear"></div>
</div>
<!--end main-->
