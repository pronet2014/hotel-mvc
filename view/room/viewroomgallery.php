<!--main-->
<div class="main">
  <div class="container">
    <ol class="breadcrumb" style="margin-bottom: 15px;">
      <li><a href="<?php echo $system->base_url(); ?>">Home</a></li>
      <li><a href="<?php echo $system->base_url(); ?>/room/gallery">Galeri Kamar</a></li>
      <li class="active"><?php echo $data['id']; ?></li>
    </ol>

    <div class="col-sm-12 clr" style="text-align: center; margin-bottom: 10px;">
      <h2>Data Galeri Kamar</h2> <a href="<?php echo $system->base_url(); ?>/room/gallery/add">Tambah Galeri Kamar</a>
    </div>

    <div class="row">
      <?php foreach ($data[picture] as $row): ?>
          <div class="col-md-3" style="margin-bottom: 30px;">
              <?php echo "<a href='".$system->base_url()."/room/gallery/edit/$row'><img src='" . $system->base_url() . "/assets/images/room/" . $row . ".jpg' height='200px'></a>"; ?>
          </div>
      <?php endforeach; ?>
    </div>

	</div>
	<div class="clear"></div>
</div>
<!--end main-->
