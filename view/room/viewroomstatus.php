<!--main-->
<style>
    .room-container {
        margin: 8px;
        padding: 6px;
        text-align: center;
        color: white;
        font-size: 2.5em;
        background: green;
    }

    .room-container.disabled {
        background-color: red;
    }
</style>
<div class="main">
    <div class="container">
        <ol class="breadcrumb" style="margin-bottom: 15px;">
            <li><a href="<?php echo $system->base_url(); ?>">Home</a></li>
            <li><a href="<?php echo $system->base_url(); ?>/room">Kamar</a></li>
            <li class="active">Status</a></li>
        </ol>

        <div class="col-sm-12 clr" style="text-align: center; margin-bottom: 10px;">
            <h2>Status Kamar</h2>
        </div>

        <div id="info">
            <div class="row">
                <a type="button" id="reload-status" class="btn">Force Reload</a>
            </div>
            <div class="row">
                <?php
                foreach ($data as $row) {
                    $class = ($row[status] == 0) ? "disabled" : "";
                    echo "<div class='col-xs-1 room-container $class' data-room-number={$row[name]}>$row[name]</div>";
                }
                ?>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<!--end main-->

<script>
    var wasError = false;
    function reloadRoomStatus() {
        $.ajax({
            url: location.href,
            data: {json: true},
            method: "GET",
            dataType: "json"
        })
            .success(function (data) {
                wasError = false;
                var i;
                for (i = 0; i < data.length; i++) {
                    var obj = $("#info div[data-room-number=" + data[i].name + "]");
                    var currentStatus = $(obj).hasClass("disabled");
                    if (data[i].status == 0 && currentStatus == false)
                        $(obj).addClass('disabled');
                    else if (data[i].status == 1 && currentStatus == true)
                        $(obj).removeClass("disabled");
                }
            })
            .fail(function() {
                if(wasError == false) {
                    alert("Error getting data from server. Please contact administrator or check your connection.");
                    wasError = true;
                }
            });
    }
    $(document).ready(function () {
        setInterval(reloadRoomStatus, 3000);
        $("#reload-status").on('click', reloadRoomStatus);
    });
</script>
