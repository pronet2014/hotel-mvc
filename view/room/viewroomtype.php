<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/u/bs/dt-1.10.12/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/u/bs/dt-1.10.12/datatables.min.js"></script>

<!--main-->
<div class="main">
  <div class="container">
    <ol class="breadcrumb" style="margin-bottom: 15px;">
      <li><a href="<?php echo $system->base_url(); ?>">Home</a></li>
      <li class="active"> Tipe Kamar</a></li>
    </ol>

    <div class="col-sm-12 clr" style="text-align: center; margin-bottom: 10px;">
      <h2>Data Tipe Kamar</h2> <a href="<?php echo $system->base_url(); ?>/room/type/add">Tambah Tipe Kamar</a>
    </div>

		<table id="roomtype-tb" class="table table-striped table-bordered data">
			<thead>
				<tr><th>ID</th><th>Nama Tipe</th><th>Harga</th><th width="40%">Fitur</th><th>Gambar</th></tr>
			</thead>
			<tbody>
				<?php
          foreach($data as $row) {
            echo "<tr><td>$row[id]<br><a href='".$system->base_url()."/room/type/details/". strtolower(preg_replace('/ /', '-', $row[name]))."'>Kamar</a> | <a href='".$system->base_url()."/room/gallery/details/". strtolower(preg_replace('/ /', '-', $row[name]))."'>Galeri</a><br><a href='".$system->base_url()."/room/type/edit/". strtolower(preg_replace('/ /', '-', $row[name]))."'>Edit</a> | <a class='hapus' style=\"cursor: pointer;\" room=\"$row[id]\">Hapus</a>
            </td><td>$row[name]</td><td>IDR ".number_format($row[price], 0, ",", ".")."</td><td>$row[feature]</td><td><img src=\"".$system->base_url()."/assets/images/room/$row[picture].jpg\" width=\"250px\" alt=\"\"></td></tr>";
          }
				?>
			</tbody>
		</table>
	</div>
	<div class="clear"></div>
</div>
<!--end main-->

<script>
  $(document).ready(function(){
    $('#roomtype-tb').DataTable();

    $('a.hapus').on('click', function() {
      if(confirm('Apakah Anda yakin akan menghapus tipe kamar ini?') == true) {
        $.ajax( {
          type: "POST",
          url: '<?php echo $system->site_url('room/type/delete'); ?>',
          data: 'id='+$(this).attr('room'),
          cache: false,
          success: function(data) {
            if(data == "success")
              window.location.reload();
            else
              alert('Terdapat kesalahan sehingga tidak dapat melakukan penghapusan data!');
          }
        });
      }
    });
  });
</script>
