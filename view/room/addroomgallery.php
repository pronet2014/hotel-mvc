<link href="<?php echo $system->base_url(); ?>/assets/css/fileinput.min.css" rel="stylesheet" type="text/css" media="all" />

<!--main-->
<div class="main">
  <div class="container">
    <ol class="breadcrumb" style="margin-bottom: 15px;">
      <li><a href="<?php echo $system->base_url(); ?>">Home</a></li>
      <li><a href="<?php echo $system->base_url(); ?>/room/gallery">Galeri Kamar</a></li>
      <li class="active">Tambah</li>
    </ol>

    <div class="col-sm-offset-2 col-sm-10 clr" style="margin-bottom: 10px;">
      <h2>Tambah Galeri Kamar</h2>
      <div id="notification" class="alert" style="display: none"></div>
    </div>

    <form class="form-horizontal" method="post" enctype="multipart/form-data">
			<div class="form-group">
        <label for="type" class="col-sm-2 control-label">Tipe</label>
        <div class="col-sm-10">
          <select class="form-control" id="type" name="type">
            <?php
              foreach($data as $room)
                echo "<option value=\"$room[id]\">$room[name]</option>";
            ?>
          </select>
        </div>
      </div>
			<div class="form-group">
				<label for="input-2" class="col-sm-2 control-label">Gambar</label>
				<div class="col-sm-10">
					<input id="input-2" name="input2[]" type="file" class="file" multiple data-show-caption="true" data-allowed-file-types='["image"]'>
				</div>
			</div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button id="submit" class="btn btn-default">Submit</button>
        </div>
      </div>
    </form>
	</div>
	<div class="clear"></div>
</div>
<!--end main-->

<script src="<?php echo $system->base_url(); ?>/assets/js/fileinput.min.js"></script>
<script>
$(document).ready(function(){
  $("#input-2").fileinput({
    uploadAsync: false,
    uploadUrl: location.href,
    uploadExtraData: function() {
      return {
        type: $("input[name=type]").val()
      };
    }
  });
});
</script>
