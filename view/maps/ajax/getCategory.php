<?php
  error_reporting(0);
  $req = $_GET['CatID'];
  if(is_null($req) || empty($req))
    $req = true;
  if(!is_numeric($req) && is_string($req)) {
    http_response_code(404);
    exit();
  }
  $db = mysqli_connect("localhost", "root", "", "maps_recommendation");
  $q  = $db->prepare("SELECT * FROM rec_category WHERE ID ".(is_bool($req) ? ' >= ' : ' = ')." ?");
  $q->bind_param('i', $req);
  if($q->execute()) {
    $r  = $q->get_result();
    if($r === FALSE)
      http_response_code(500);
      else {
        while($row = $r->fetch_assoc()) {
          $arrRes[$row['ID']] = $row;
        }
        echo json_encode($arrRes);
      }
  } else {
    http_response_code(500);
  }
