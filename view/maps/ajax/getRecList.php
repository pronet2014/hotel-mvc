<?php
  error_reporting(0);
  $req = $_GET['CatID'];
  if(is_null($req) || empty($req))
    $req = true;
  if(!is_numeric($req) && is_string($req)) {
    http_response_code(404);
    exit();
  }
  $db = mysqli_connect("localhost", "root", "", "maps_recommendation");
  $q  = $db->prepare("SELECT * FROM reclist WHERE Cat_ID ".(is_bool($req) ? '>=' : '=')." ?");
  $q->bind_param('i', $req);
  if($q->execute()) {
    $r  = $q->get_result();
    if($r === FALSE)
      http_response_code(500);
      else {
        echo json_encode($r->fetch_all(MYSQLI_ASSOC));
      }
  } else {
    http_response_code(500);
  }
