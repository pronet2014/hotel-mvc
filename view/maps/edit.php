<style>
    fieldset {
        margin-top: 2em;
    }
    form .row {
        margin: 1em 0;
    }
    textarea {
        min-width: 100%;
        max-width: 100%;
    }
    #inputMap {
        width: 100%;
        height: 20em;
    }
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <fieldset>
                <legend>Form Input Rekomendasi</legend>
                <form class="container-fluid form" id="input-form" method="post">
                    <div class="row">
                        <div class="col-md-2 text-left">
                            <label>Nama</label>
                        </div>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="Nama" value="<?php echo $data['Nama']; ?>" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 text-left">
                            <label>Alamat</label>
                        </div>
                        <div class="col-md-10">
                            <textarea class="form-control" name="Alamat" cols="50" required><?php echo $data['Alamat']; ?></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 text-left">
                            <label>Deskripsi</label>
                        </div>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="Deskripsi" value="<?php echo $data['Deskripsi']; ?>" required/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 text-left">
                            <label>Lokasi</label>
                        </div>
                        <div class="col-md-10">
                            <div id="inputMap">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 text-left">
                            <label>Jarak</label>
                        </div>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="Jarak" value="<?php echo $data['Jarak']; ?>" required/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 text-left">
                            <label>Nilai Prioritas</label>
                        </div>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="Nilai_Prioritas" value="<?php echo $data['Nilai_Prioritas']; ?>" required/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 text-left">
                            <label>Kategori</label>
                        </div>
                        <div class="col-md-10">
                            <select class="form-control" size=5 name="Kategori[]" multiple="multiple">
                                <?php
                                foreach($kategori as $item) {
                                    echo "<option value=".$item['Id_Kategori'].">{$item['Nama_Kategori']}</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 text-right">
                            <input class="btn" type="submit" value="Simpan"/>
                        </div>
                        <div class="col-xs-6 text-left">
                            <input class="btn" type="reset" value="Reset"
                                   onclick="return confirm('hapus data yang telah diinput?')">
                        </div>
                    </div>
                </form>
            </fieldset>
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsyBVLC_bZURZKs2wvJtGlDHuE6sDU6gg&"></script>
<script>
    var maps, marker, lat, lng;
    $(document).ready(function() {
        lat = <?php echo $data[Latitude]; ?>;
        lng = <?php echo $data[Longitude]; ?>;
        var selected = <?php echo json_encode($selectedKategori) ?>;
        for(i = 0; i < selected.length; i++) {
            $("select[name='Kategori[]'] option[value=" + selected[i][0] + "]").prop("selected", true);
        }
        maps = new google.maps.Map(document.getElementById('inputMap'), {
            center: {lat: lat , lng: lng},
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        });
        marker = new google.maps.Marker({
            position: maps.getCenter(),
            animation: google.maps.Animation.DROP,
            map: maps,
            draggable: true
        });
        marker.addListener('drag', function() {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        });
        marker.addListener('dragend', function() {
            lat = marker.getPosition().lat();
            lng = marker.getPosition().lng();
            marker.setAnimation(google.maps.Animation.DROP);
        });
        $("#input-form").submit(function() {
            $(this).append("<input type=hidden name=Lat value='" + lat + "'>");
            $(this).append("<input type=hidden name=Lng value='" + lng + "'>");
        });
    });
</script>
