<!--Maps and Recommendations-->
<style>
    #recMap {
        max-height: 80%;
        margin: 0em auto 1em auto;
        transition: all 1s ease;
    }

    #recMap.activated {
        height: 80%;
    }

    .title-map { font-size: 2em; }
    .desc-map p { font-size: 1em; }
    .maps h3 { text-transform: uppercase; color: #777;  }
    .cat-item img {
        max-width: 3em;
        max-height: 2em;
        vertical-align: middle;
        display: inline-block;
        margin-right: 1em;
    }

    #catList { overflow-y: auto; }

    #filter-row { display: none; margin: 1em auto 2em auto; }
    #distance-slider {
        position: relative;
        width: 100%;
    }
    #search-button {
        margin: 1em;
    }
    #search-button:hover {
        background: #53c853;
        color: #fefefe;
    }
    a .cat-item {
        padding: 0.5em;
        cursor: pointer;
        color: #333;
    }
    a .cat-item:hover {
        background: #EEE;
    }
    a.map-cat-button.selected > .cat-item {
        background: #D3691E;
        color: #FFFFFF;
    }
    .desc-map i {
        font-style: italic;
    }
    .glyphicon {
        vertical-align: middle;
        margin-right: 0.25em;
    }
    input[type=range] {
        background: inherit;
    }
</style>
<div class="main container-fluid maps">
    <div class="row">
        <div class="col-md-12">
            <h2 style="text-align: center; margin-bottom: 2em" class="head">Recommendations</h2>
        </div>
    </div>
    <div class="row">
    </div>
    <div class="row">
        <div class="col-md-2 col-md-offset-2">
            <div class="row">
                <div class="col-md-12">
                    <input class="form-control" type="text" name="Rec[Name]" placeholder="Search Places (ex. PDAM Tirtanadi)">
                </div>
                <div class="col-md-12 text-center">
                    <button class="btn" id="search-button">Search</button>
                </div>
                <div class="col-md-12 text-center">
                    <a id="showMore" type="button" style="cursor: no-drop"><span class="glyphicon glyphicon-triangle-bottom"></span> Show More Filters</a>
                </div>
            </div>
            <div class="row" id="filter-row">
                <div class="col-md-12">
                    <h3 class="head" style="text-align: center">Categories</h3>
                    <div id="catList"></div>
                </div>
                <div class="col-md-12">
                    <h3 class="head" style="text-align: center">Distance</h3>
                    <div id="distance-slider"></div>
                    <div id="distance-range-label" class="text-center"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsyBVLC_bZURZKs2wvJtGlDHuE6sDU6gg&"></script>
            <script type="text/javascript">
                var recommendations, json, markers = {}, infowindow = {}, currentPin = -1, catData;
                // Fungsi untuk membuat styling dan tag untuk judul
                function getTitleContent(title, desc, addr, dist) {
                    console.log(typeof addr);
                    return "<div>" +
                        "<h1 class=\"title-map\">" + title + "</h1>" +
                        "<div class=\"desc-map\">" +
                        "<p><i>" + addr + " " + (addr.length == 0 ? dist : "(" + dist + ")" ) + "</i></p>" +
                        "<p>" + desc + "</p>" +
                        "</div>" +
                        "</div>";
                }
                // Fungsi untuk membuat styling dan tag untuk daftar kategori
                function getCategoryHTML(id, icon, name, selected = false) {
                    return "<a class='map-cat-button" + (selected ? " selected" : "") +"' type=button data-map-id='" + id + "'><div class=\"col-md-12 cat-item\"><img class='category_img' src='" + <?php echo json_encode($system->base_url()) ?> +"/" + icon + "' >" + name + "</div>";
                }
                // Inisialisasi map yang akan dieksekusi setiap kali data json diperbaharui menggunakan AJAX
                function initMap() {
                    var i; // Variabel untuk perulangan pada script ini
                    var pos = {}; // pos menyimpan daftar latLng lokasi pada peta
                    for (var key in json) {
                        // Membuat objek LatLng untuk setiap lokasi dan memasukkannya ke dalam array pos
                        pos[key] = (new google.maps.LatLng(parseFloat(json[key].Latitude), parseFloat(json[key].Longitude)));
                    }
                    if (typeof recommendations == 'undefined') { // Membuat map baru hanya jika recommendations belum
                        // diinisialisasi
                        // Menginisialisasi peta recommendations baru
                        recommendations = new google.maps.Map(document.getElementById('recMap'), {
                            center: {lat: 3.566533, lng: 98.659601}, // Menentukan titik tengah
                            zoom: 15,														// Menentukan nilai zoom awal
                            mapTypeId: google.maps.MapTypeId.ROADMAP, // Menentukan tipe peta yang ditamppilkan
                            scrollwheel: false										// Menonaktifkan scroll yang berfungsi untuk
                            // melakukan zomming pada peta menggunakan scroll
                        });
                        $("#recMap").addClass('activated');
                    }
                    for (var key in json) {
                        // Membuat dan memasukkan marker ke dalam peta dan ke dalam array markers
                        markers[key] = (new google.maps.Marker({
                            id: key,
                            position: pos[key],
                            map: recommendations,
                            title: json[key].Nama,
                            icon: new google.maps.MarkerImage(
                                <?php echo json_encode($system->base_url()) ?> +"/" + catData[json[key].Id_Kategori].Lokasi_Gambar,
                                null,
                                null,
                                new google.maps.Point(16, 50),
                                new google.maps.Size(30, 50)
                            )
                        }));
                        // Membuat dan memasukkan InfoWindow baru yang akan digunakan untuk menampilkan informasi
                        // tambahan saat marker diklik
                        infowindow[key] = (new google.maps.InfoWindow({
                            content: getTitleContent(json[key].Nama, (json[key].Deskripsi ? json[key].Deskripsi : "No description."),
                                (json[key].Alamat ? json[key].Alamat : ""), (parseInt(json[key].Jarak) >= 1000 ?
                                    (parseInt(json[key].Jarak) / 1000 + " km") : (parseInt(json[key].Jarak) + " m")))
                    }))
                        ;
                        // Menambahkan listener click pada marker agar InfoWindow yang diinginkan muncul
                        markers[key].addListener('click', function () {
                            infowindow[this.id].open(recommendations, this); // Membuka InfoWindow pada lokasi marker pada peta recommendations
                            if (currentPin != -1) // Jika pengguna sudah pernah mengeklik marker pada peta
                                infowindow[currentPin].close(); // Tutup InfoWindow yang sudah dibuka sebelumnya
                            currentPin = this.id; // Simpan id marker terakhir yang dibuka
                            recommendations.panTo(this.getPosition()); // Pindahkan fokus pada peta recommendations ke titik tengah peta
                            // Memakai panTo agar peta memindahkan fokus dari titik sebelumnya ke
                            // titik yang baru. setCenter juga dapat melakukan fungsi ini, akan tetapi
                            // peta recommendations diperbaharui, bukan memindahkan fokus.
                        });
                    }
                }

                function filterSearch() {
                    for (var key in markers) {
                        google.maps.event.clearInstanceListeners(infowindow[key]);
                        markers[key].setMap(null); // Menghapus markers dan InfoWindow yang lama
                    }
                    markers = {};
                    infowindow = {};
                    var min = $("#distance-slider").slider('option', 'values')[0], max = $("#distance-slider").slider('option', 'values')[1];
                    console.log(max == 0);
                    // Melakukan panggilan AJAX untuk mengambil daftar recommendations dengan kategori tertentu
                    $.ajax({
                        url: '<?php echo $system->site_url('recommendation/get_list') ?>',
                        method: 'GET',
                        data: {
                            CatID: $(".map-cat-button.selected").data('map-id'),
                            Name: $("input[name='Rec[Name]'").val(),
                            Range: {
                                Min: min,
                                Max: (max == 0 ? -1 : max)
                            }
                        },
                        dataType: 'json',
                        async: false
                    }).success(function (data) {
                        if(min == 0 && max == 0 && json == null) {
                            $("#distance-slider").slider('option', 'min', data.minDistance);
                            $("#distance-slider").slider('option', 'max', data.maxDistance);
                            $("#distance-range-label").html(data.minDistance + "m - " + data.maxDistance + " m");
                            $("#distance-slider").slider('option', 'values', [data.minDistance, data.maxDistance]);
                        }
                        json = data.list; // Memperbaharui data json
                        initMap(); // Inisialisasi kembali markers untuk recommendations
                    });
                }

                $(document).ready(function () {
                    // Memanggil fungsi AJAX untuk mengambil data kategori dari halaman lain secara  sinkron
                    $("#showMore").click(function() {
                        $("#filter-row").slideToggle();
                    });
                    $("#distance-slider").slider({
                        range: true,
                        animate: true,
                        max: 8888888,
                        slide: function(event, ui) {
                            $("#distance-range-label").html(ui.values[0] + "m - " + ui.values[1] + " m");
                        }
                    });
                    $.ajax({
                        url: '<?php echo $system->site_url('recommendation/get_category') ?>',
                        method: 'GET',
                        dataType: 'json',
                        async: false
                    }).success(function (data) {
                        $("#catList").html(""); // Menghapus seluruh data kategori jika ada
                        // dan menambahkan kategori pada catList (dan juga all categories)
                        $("#catList").append(getCategoryHTML('', 'assets/images/recommendations/all.png', 'All Categories', true));
                        for (var key in data) {
                            if (data.hasOwnProperty(key))
                                $("#catList").append(getCategoryHTML(key, data[key].Lokasi_Gambar, data[key].Nama_Kategori));
                        }
                        // Membuat listener click untuk setiap tombol kategori
                        $(".map-cat-button").each(function (i, e) {
                            $(e).on('click', function () {
                                $(".map-cat-button.selected").removeClass('selected');
                                $(e).addClass('selected');
                            });
                        });
                        catData = data; // Menyimpan data kategori sehingga tidak perlu meminta berulang-ulang
                    });
                    $("#search-button").on('click', filterSearch);
                    filterSearch();
                });
            </script>
            <div id="recMap" class="maps-container"></div> <!-- Tag div yang digunakan oleh maps -->
        </div>
    </div>
</div>

