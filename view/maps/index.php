<?php
    function halaman($page, $noPage)
    {
        if ($page == $noPage)
            echo " <a href='#'><u>".$page."</u></a> ";
        else
            echo " <a href='".$_SERVER['PHP_SELF']."?page=".$page."'>".$page."</a> ";
    }

    $dataPerPage = 5;

    if(isset($_GET['page']))
        $noPage = $_GET['page'];
    else $noPage = 1;

    $offset = ($noPage - 1) * $dataPerPage;
?>
<style>
    tbody > tr:nth-child(2n+1) > td, tbody > tr:nth-child(2n+1) > th {
        background-color: #ededed;
    }

    table {
        margin: auto;
        border-collapse: collapse;
        box-shadow: darkgrey 3px;
    }

    thead tr {
        background-color: #36c2ff;
    }
    table td, table th {
        padding: 0.5em;
        vertical-align: middle;
    }

    .input-recommendation {
        display: inline-block;
        padding: 0.5em;
        text-align: center;
        width: 100%;
        margin: 0.5em;
    }

    .input-recommendation .glyphicon {
        margin-right: 0.5em;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1 align="center">Tabel Rekomendasi</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <a href="<?php echo $system->site_url('recommendation/insert'); ?>" class="input-recommendation"><span class="glyphicon glyphicon-plus"></span> Input
                Rekomendasi</a>
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>ID Rekomendasi</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Deskripsi</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Jarak</th>
                    <th>Nilai Prioritas</th>
                    <th>Pilihan</th>
                </tr>
                </thead>

                <tbody>
                <?php
                foreach ($tempat as $data) :
                    ?>
                    <tr>
                        <td><?php echo ++$i; ?></td>
                        <td><?php echo $data['Nama'] ?></td>
                        <td><?php echo $data['Alamat'] ?></td>
                        <td><?php echo $data['Deskripsi'] ?></td>
                        <td><?php echo $data['Latitude'] ?></td>
                        <td><?php echo $data['Longitude'] ?></td>
                        <td><?php echo ($data['Jarak'] >= 1000) ? strval($data['Jarak'] / 1000)." km" : strval($data['Jarak'])." m" ?></td>
                        <td><?php echo $data['Nilai_Prioritas'] ?></td>
                        <td align="center">
                            <a href="<?php echo $system->site_url('recommendation/edit/' . $data['Id_Rekomendasi']) ?>">Edit</a>

                            <a href="<?php echo $system->site_url('recommendation/delete/' . $data['Id_Rekomendasi']) ?>"
                               onclick="return confirm('Anda yakin akan menghapus data?')">Hapus</a>
                        </td>
                    </tr>
                    <?php
                endforeach;
                ?>
                </tbody>
            </table>
            <div class="text-center">
                <ul class="pagination">
                    <?php
                        function printPage($num) {
                            return "<li><a href='".(\System\KomA::app()->site_url('recommendation/manage/'.$num))."'>".
                                    $num."</a></li>";
                        }
                        $pageCount = ceil($paging['total'] / 5);
                        if($paging['page'] != 1)
                            echo "<li><a href='#'> Previous <i class='glyphicon glyphicon-circle-arrow-left'></i></a></li>";
                        for($i = $paging['page'] - 3; $i <= $paging['page']; $i++) {
                            if($i < 1)
                                continue;
                            else if($i > $pageCount)
                                break;
                            echo printPage($i);
                        }
                        for($i = $paging['page'] + 1; $i <= $paging['page'] + 2; $i++) {
                            if($i > $pageCount)
                                break;
                            echo printPage($i);
                        }
                        if($pageCount != $paging['page'])
                            echo "<li><a href='#'> Next <i class='glyphicon glyphicon-circle-arrow-right'></i></a></li>";

                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>
