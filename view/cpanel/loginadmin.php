<html>
<head>
	<title>KOM-A HOTEL RESORT - FACILITIES</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/form.css" rel="stylesheet" type="text/css" media="all" />
	<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="js/jquery1.min.js"></script>
	
		<!-- start menu -->
	<link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="js/megamenu.js"></script>
	<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
		<!-- end start menu -->
	
		<!--start slider -->
    <link rel="stylesheet" href="css/fwslider.css" media="all">
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/css3-mediaqueries.js"></script>
    <script src="js/fwslider.js"></script>
		<!--end slider -->
		
	<script src="js/jquery.easydropdown.js"></script>
</head>
<body>
	<!--TOP HEADER-->
	<div class="header-top">
		<div class="wrap"> 
			<div class="header-top-left">
				<div class="box">
					<img class="call" src="picture/images/numbercall.png" height="30px">
				</div>
				<div class="clear">
				</div>
			</div>
			<div class="cssmenu">
				<ul>
					<li><a href="#"></a></li> |
					<li><a href="#" style="background-color:black;padding:2px;">Log In</a></li> |
					<li><a href="sign-up.html">Sign Up</a></li> |
					<li><a href="recervacy.html">BOOK NOW</a></li> |
				</ul>
			</div>
			<div class="clear">
			</div>
		</div>
	</div>
	<!--end TOP HEADER-->
	
	<!--BOTTOM HEADER-->
	<div class="header-bottom">
	    <div class="wrap">
			<div class="header-bottom-left">
				<div class="logo">
					<a href="#"><img src="picture/images/log.png" alt="" height="50px"/></a>
				</div>
				<div class="menu">
					<ul class="megamenu skyblue">
						<li><a href="index.html">HOME</a></li>
						<li><a class="color1" href="recommendation.html">RECOMMENDATION</a></li>
						<li><a class="color2" href="room.html">ROOM</a></li>
						<li><a class="color3" href="facility.html">FACILITIES</a></li>
						<li><a class="color4" href="about.html">ABOUT US</a></li>
					</ul>
				</div>
			</div>
			<div class="header-bottom-right">
				<div class="search">	  
					<input type="text" name="s" class="textbox" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
					<input type="submit" value="Subscribe" id="submit" name="submit">
					<div id="response"></div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<!--end BOTTOM HEADER-->
	
	<!-- start slider -->
    <div id="fwslider">
        <div class="slider_container">
            <div class="slide"> 
                <!-- Slide image -->
                    <img src="picture/pict/ban3.jpg" alt=""/>
                <!-- /Slide image -->
                <!-- Texts container -->
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h1 class="title">WELCOME to</h4>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
                 <!-- /Texts container -->
            </div>
            <!-- /Duplicate to create more slides -->
            <div class="slide">
                <img src="picture/pict/ban2.jpg" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                       <!-- Text title -->
                        <h1 class="title">WELCOME to</h4>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
            </div>
            <!--/slide -->
			 <!-- /Duplicate to create more slides -->
            <div class="slide">
                <img src="picture/pict/ban1a.jpg" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                       <!-- Text title -->
                        <h1 class="title">WELCOME to</h4>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
            </div>
            <!--/slide -->
        </div>
        <div class="timers"></div>
        <div class="slidePrev"><span></span></div>
        <div class="slideNext"><span></span></div>
    </div>
	<!--end SLIDER-->
	
	<!--list HEADER-->
	<div class="header-list">
 	</div>
	<!--end list HEADER-->
	
	
	<div class="login">
		<div class="col_1_of_login span_1_of_login">
			<div class="login-title">
				<h4 class="title"><center>LOG IN</center></h4>
					<div id="loginbox" class="loginbox">
						<form action="aksi_login.php" method="post" name="loginadmin" id="login-form">
							<fieldset class="input">
								<p id="login-form-username">
									<label for="modlgn_username">Username</label>
									<input id="modlgn_username" type="text" name="username" class="inputbox" size="18" autocomplete="off">
								</p>
								<p id="login-form-password">
									<label for="modlgn_passwd">Password</label>
									<input id="modlgn_passwd" type="password" name="password" class="inputbox" size="18" autocomplete="off">
								</p>
								<div class="remember">
									<p id="login-form-remember">
										<label for="modlgn_remember"><a href="#">Forget Your Password ? </a></label>
									</p>
									<input type="submit" name="adminlogin" class="button" value="Login As Admin"><div class="clear"></div>
								</div>
							</fieldset>
						</form>
					</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	
	<!--footer-->
	<div class="footer">
		<div class="footer-bottom">
			<div class="copy">
				<p>© 2016 Template by. <a href="index.html" target="_blank">KOM-A TI USU 2014</a></p>	
			</div>
			<img class="footer_logo" src="picture/images/logofooter.png">
			<div class="clear"></div>
		</div>
	</div>
	
</body>
</html>