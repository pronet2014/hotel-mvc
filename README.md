# hotel-mvc #

Repo ini menampung file untuk tugas Pronet II 2014 secara keseluruhan. 


## Requirements ##

* ** HTTP Server ** dengan PHP dan ekstensinya 
* ** MySQL ** dan Database usu_pronet2 


## Konfigurasi ##

Ubahlah konfigurasi pada system/config/settings.php. 
```
#!php
<?php

return [
    'baseUrl' => 'http://it.hotel',
    'defaultController' => ''
];
```

Ubahlah konfigurasi pada system/config/database.php sesuai dengan konfigurasi database anda.

```
#!php
<?php
return [
    'DB_SERVER' => 'localhost',
    'DB_USERNAME' => 'root',
    'DB_PASSWORD' => '',
    'DB_NAME' => 'usu_pronet2',
];
```

# MVC (Model-View-Controller) #

Repo ini menggunakan arsitektur MVC. Silahkan pelajari terlebih dahulu cara bekerja MVC melalui http://bfy.tw/6LWu.

## Controller ##

Anda hanya perlu membuat sebuah file PHP baru dengan nama yang sama dengan nama controller yang akan anda buat. Nama file yang anda berikan haruslah **dengan menggunakan huruf kecil (lowercase)**. Nama controller dapat berupa camelCase atau cara penamaan favorit anda.


```
#!php

<?php

namespace Controller;

class Site extends Controller
{
    public function defaultAction() { return 'index'; }

    public function index()
    {
//        include 'view/me.php';
//               or
//        $this->render('view/me.php');
    }
}

?>

```

## Model ##

Untuk sementara, model hanya memiliki parent yang mengambil interface MySQLi dan menyimpannya untuk model tersebut. Untuk mengakses MySQLi pada Model, cukup mengakses $this->db melalui model. 

## View ##

View dapat diletakkan pada folder view atau folder kesayangan anda. Saat ini, belum ada fungsi untuk melakukan rendering view. 

# Changelogs #

**25 Juni 2016**

- PR - Update login untuk karyawan dari PayrollHRD ke Control Panel
- Pemindahan fungsi getUsername dari PayrollHRD ke WorkerModel
- PR - Update pemakaian session untuk controller PayrollHRD
- CP - Penambahan controller CP

**24 Juni 2016**

- PR - Penambahan suggestion karyawan pada search.

**23 Juni 2016**

- PR - Perbaikkan update di model controler view (update sudah bisa berjalan)
- PR - penambahan css bootstrap 3.3.1 secara offline dikarenakan jika memakai yang online bootstrap tidak mau berjalan
- Perbaikan untuk masalah routeURI yang tidak dapat digunakan jika hanya menggunakan localhost dan harus menggunakan Virtual Host

**22 Juni 2016**

- Tampilan error sementara, dapat diakses menggunakan fungsi show_error pada KomA
- WorkerModel sementara
- RM - Tampilan status room dan request menggunakan AJAX dan JSON
- PR - akhirsesi dan mulaisesi yang tidak digunakan lagi.
- PR - Perbaikkan update di view model dan controller oleh Ezzay
- PR - Perbaikkan session,
- PR - Perbaikan beberapa view model controller
- CP - Hapus file assets dkk. dari tim CP di folder View
- RM - Menambah library fileinput
- RM - Menambah beberapa fitur CRUD.
- CP - View untuk tampilan control panel

**21 Juni 2016**

- PR - Perbaikan untuk css Payroll dan HRD di view
- PR - Membuat fungsi update di controller, model.
- PR - Menambah view update.
- PR - Perbaikan untuk Payroll dan HRD (model, controller, dan view) 3rd
- PR - Finishing session

**20 Juni 2016**

- Class Controller dan View untuk rendering. Dapat diabaikan jika tidak diperlukan. 
- Dokumentasi untuk beberapa class
- Pengecekan baseUrl pada KomA, jika tidak tersedia, maka kembalikan alamat server.
- CSS - Penambahan atribut !important untuk font-family pada body
- RM - Penambahan style pada CSS
- RM - Pembaruan gambar room
- RM - Pembaruan manajemen file view
- RM - Penambahan detail room
- PR - Mengganti permission file MVC
- PR - Mengganti nama file agar sesuai dengan model yang ada
- PR - Tambahan hasil pekerjaan Kelompok 7 dan perubahan driver database ke MySQLi

**19 Juni 2016 - MVC 1.2 Alpha 1**

- It's Singleton! Sedikit kurang mengerti, tapi intinya adalah satu objek untuk semuanya. Sekarang, segala sesuatu yang berkenaan dengan method dan komponen dasar seperti database dan base_url, site_url, redirect, dkk. dapat dilihat atau diubah atau digunakan melalui class KomA!
- Modifikasi controller, model, dan view untuk singleton terbaru.
- Penghapusan class Controller, karena untuk sementara, tidak digunakan.

**19 Juni 2016 - RM v1.2**

- Pembaruan gambar ruangan yang digunakan
- Penambahan penggunaan namespace pada controller
- Mengganti nama method pada controller
- Mengganti seluruh relative path pada snippet link css dan js menjadi absolute


**19 Juni 2016 - MVC 1.1 A1**

- Controller, view, dan model recommendation
- Penggabungan pictures dengan images (nested images folder)
- Penghapusan konfigurasi .idea
- Konstanta baru __CONTROLLER__ dan __ACTION__ untuk identifikasi controller dan action yang sedang diakses untuk penggunaan di masa depan.
- Penggunaan sementara halaman error
- File konfigurasi (system/settings.php), masih belum tahu cara penggunaannya
- Class controller untuk dapat diextend semua class controller lainnya

** 18 Juni 2016 - MVC 1**

- MVC buatan bang Alex Wijaya, melalui https://bitbucket.org/k2pronetiikoma2014/room_management
- Routing sederhana dan autoload

** 18 Jun 2016 - Pembuatan Repository**